// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Data.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 17:27:12 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 17:54:51 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef DATA_HPP
# define DATA_HPP

# include <iostream>
# include <ctime>
# include <cstdlib>

struct Data
{
	std::string s1;
	int n;
	std::string s2;
};

#endif
