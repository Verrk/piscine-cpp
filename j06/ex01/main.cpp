// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 17:27:58 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 18:01:29 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Data.hpp"

void randStr(char *str)
{
	char c;
	int i = 0;

	while (i < 8)
	{
		c = rand() % 128;
		if (isalnum(c))
			str[i++] = c;
	}
	str[i] = '\0';
}

void * serialize()
{
	char *raw = new char[21];
	int n = rand();
	int *rawn;
	int i;
	char s1[9];
	char s2[9];

	randStr(s1);
	randStr(s2);
	std::cout << "Random value genereted:" << std::endl;
	std::cout << "s1: " << s1 << std::endl;
	std::cout << "n: " << n << std::endl;
	std::cout << "s2: " << s2 << std::endl;
	for (i = 0; i < 8; i++)
		raw[i] = s1[i];
	rawn = reinterpret_cast<int*>(&raw[8]);
	*rawn = n;
	for (i = 0; i < 8; i++)
		raw[i + 12] = s2[i];
	raw[20] = '\0';
	return raw;
}

Data * deserialize(void *raw)
{
	Data * d = new Data;
	char * str = reinterpret_cast<char*>(raw);
	int * rawn;

	for (int i = 0; i < 8; i++)
		d->s1 += str[i];
	rawn = reinterpret_cast<int*>(&str[8]);
	d->n = *rawn;
	for (int i = 12; i < 20; i++)
		d->s2 += str[i];
	return d;
}

int main()
{
	srand(time(NULL));
	Data * d;
	void * raw;
	char *del;

	raw = serialize();
	d = deserialize(raw);
	std::cout << "Struct Data:" << std::endl;
	std::cout << "data->s1: " << d->s1 << std::endl;
	std::cout << "data->n: " << d->n << std::endl;
	std::cout << "data->s2: " << d->s2 << std::endl;
	del = reinterpret_cast<char*>(raw);
	delete del;
	delete d;
	return 0;
}
