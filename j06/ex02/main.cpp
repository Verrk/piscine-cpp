// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 19:15:14 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 08:14:51 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <exception>
#include <cstdlib>
#include <ctime>

class Base {public: virtual ~Base() {}};
class A: public Base {};
class B: public Base {};
class C: public Base {};

Base * generate()
{
	int i = rand() % 3;

	if (i == 0)
	{
		std::cout << "class A randolmly generated" << std::endl;
		return new A();
	}
	else if (i == 1)
	{
		std::cout << "class B randolmly generated" << std::endl;
		return new B();
	}
	else
	{
		std::cout << "class C randolmly generated" << std::endl;
		return new C();
	}
}

void identify_from_pointer(Base * p)
{
	A *a = dynamic_cast<A*>(p);
	B *b = dynamic_cast<B*>(p);

	std::cout << "identify_form_pointer: ";
	if (a != NULL)
		std::cout << "A" << std::endl;
	else if (b != NULL)
		std::cout << "B" << std::endl;
	else
		std::cout << "C" << std::endl;
}

void idA(Base & p)
{
	try
	{
		A & a = dynamic_cast<A&>(p);
		(void)a;
		std::cout << "A" << std::endl;
	}
	catch(std::exception &e)
	{
		(void)e;
	}
}

void idB(Base & p)
{
	try
	{
		B & b = dynamic_cast<B&>(p);
		(void)b;
		std::cout << "B" << std::endl;
	}
	catch(std::exception &e)
	{
		(void)e;
	}
}

void idC(Base & p)
{
	try
	{
		C & c = dynamic_cast<C&>(p);
		(void)c;
		std::cout << "C" << std::endl;
	}
	catch(std::exception &e)
	{
		(void)e;
	}
}

void identify_from_reference(Base & p)
{
	std::cout << "identify_from_reference: ";
	idA(p);
	idB(p);
	idC(p);
}

int main()
{
	Base *p;

	srand(time(NULL));
	p = generate();
	identify_from_pointer(p);
	identify_from_reference(*p);
	delete p;
}
