// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 18:37:53 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 19:06:04 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Cast.hpp"
#include <iostream>
#include <cstdlib>
#include <iomanip>

int getPrec(std::string str)
{
	size_t len = str.find(".");

	if (len != std::string::npos)
	{
		if (str.find("f") != std::string::npos)
			len++;
		return str.size() - (len + 1);
	}
	return 1;
}

void doCast(std::string s, Cast c, double d, int prec)
{
	try
	{
		std::cout << s << ": ";
		if (s == "char")
		{
			char tmp = c.dtoa(d);
			std::cout << "'" << tmp << "'" << std::endl;
		}
		if (s == "int")
		{
			int tmp = c.dtoi(d);
			std::cout << tmp << std::endl;
		}
		if (s == "float")
		{
			float tmp = c.dtof(d);
			std::cout << std::fixed << std::setprecision(prec) << tmp << "f" << std::endl;
		}
		if (s == "double")
			std::cout << std::fixed << std::setprecision(prec) << d << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}

int main(int ac, char **av)
{
	Cast c;
	int prec = 1;
	double d;

	if (ac == 2)
	{
		std::string str(av[1]);
		if (str.size() == 3 && str[0] == '\'' && str[2] == '\'')
			d = static_cast<double>(static_cast<int>(str[1]));
		else if (str.size() == 1 && isalpha(str[0]))
			d = static_cast<double>(static_cast<int>(str[0]));
		else
		{
			d = std::atof(av[1]);
			prec = getPrec(str);
		}
		doCast("char", c, d, prec);
		doCast("int", c, d, prec);
		doCast("float", c, d, prec);
		doCast("double", c, d, prec);
	}
	else
		std::cout << "Wrong parameters" << std::endl;
}
