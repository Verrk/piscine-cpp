// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cast.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 18:25:35 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 18:37:24 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Cast.hpp"

Cast::Cast() {}

Cast::~Cast() {}

char
Cast::dtoa(double d)
{
	if (isinf(d) || isnan(d))
		throw Cast::impException();
	if (d > static_cast<double>(std::numeric_limits<char>::max()) ||
		d < static_cast<double>(std::numeric_limits<char>::min()))
		throw Cast::impException();
	if (!isprint(static_cast<int>(d)))
		throw Cast::nonDispException();
	return static_cast<char>(static_cast<int>(d));
}

int
Cast::dtoi(double d)
{
	if (isinf(d) || isnan(d))
		throw Cast::impException();
	if (d > static_cast<double>(std::numeric_limits<int>::max()) ||
		d < static_cast<double>(std::numeric_limits<int>::min()))
		throw Cast::impException();
	return static_cast<int>(d);
}

float
Cast::dtof(double d)
{
	return static_cast<float>(d);
}

const char *
Cast::impException::what() const throw()
{
	return "impossible";
}

const char *
Cast::nonDispException::what() const throw()
{
	return "Non displayable";
}
