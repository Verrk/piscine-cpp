// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cast.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 18:23:27 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 19:02:22 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CAST_HPP
# define CAST_HPP

# include <iostream>
# include <exception>
# include <limits>
# include <cmath>

class Cast
{
public:
	Cast();
	~Cast();

	char dtoa(double d);
	int dtoi(double d);
	float dtof(double d);

	class impException: public std::exception
	{
	public:
		virtual const char *what() const throw();
	};

	class nonDispException: public std::exception
	{
	public:
		virtual const char *what() const throw();
	};
};

#endif
