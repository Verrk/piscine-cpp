// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SuperTrap.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 21:50:08 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 22:21:27 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "SuperTrap.hpp"

SuperTrap::SuperTrap()
{
	_hp = FragTrap::getHp();
	_maxHp = FragTrap::getMaxHp();
	_ep = NinjaTrap::getEp();
	_maxEp = NinjaTrap::getMaxEp();
	_level = 1;
	_name = "sup3r";
	_meleeAttDmg = NinjaTrap::getMeleeAttDmg();
	_rangedAttDmg = FragTrap::getRangedAttDmg();
	_armorDmgReduc = FragTrap::getArmorDmgReduc();
}

SuperTrap::SuperTrap(std::string name):
	FragTrap(name),
	NinjaTrap(name)
{
	_hp = 100;
	_maxHp = 100;
	_ep = 60;
	_maxEp = 60;
	_level = 1;
	_name = name;
	_meleeAttDmg = 60;
	_rangedAttDmg = 20;
	_armorDmgReduc = 5;
}

SuperTrap::SuperTrap(SuperTrap const & src)
{
	*this = src;
}

SuperTrap &
SuperTrap::operator=(SuperTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

SuperTrap::~SuperTrap()
{
	std::cout << "Even in death I'm super !" << std::endl;
}

void
SuperTrap::rangedAttack(std::string const & target)
{
	FragTrap::rangedAttack(target);
}

void
SuperTrap::meleeAttack(std::string const & target)
{
	NinjaTrap::meleeAttack(target);
}

void
SuperTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
	std::cout << "Ouch !" << std::endl;
}

void
SuperTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
	std::cout << "Health !! Super !" << std::endl;
}
