// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClapTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/25 14:53:06 by cpestour          #+#    #+#             //
//   Updated: 2015/10/25 15:27:51 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

# include <iostream>

class ClapTrap
{
public:
	ClapTrap();
	ClapTrap(ClapTrap const & src);
	~ClapTrap();

	ClapTrap& operator=(ClapTrap const & rhs);

	int getHp() const;
	int getMaxHp() const;
	int getEp() const;
	int getMaxEp() const;
	int getLevel() const;
	std::string getName() const;
	int getMeleeAttDmg() const;
	int getRangedAttDmg() const;
	int getArmorDmgReduc() const;

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);

protected:
	unsigned int _hp;
	unsigned int _maxHp;
	unsigned int _ep;
	unsigned int _maxEp;
	unsigned int _level;
	std::string _name;
	unsigned int _meleeAttDmg;
	unsigned int _rangedAttDmg;
	unsigned int _armorDmgReduc;
};

#endif
