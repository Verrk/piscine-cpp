// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 10:07:23 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 22:14:04 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"

FragTrap::FragTrap()
{
	_hp = 100;
	_maxHp = 100;
	_ep = 100;
	_maxEp = 100;
	_level = 1;
	_name = "FR4G";
	_meleeAttDmg = 30;
	_rangedAttDmg = 20;
	_armorDmgReduc = 5;
	srand(time(NULL));
	std::cout << "Hi! I'm " << this->_name << "!" << std::endl;
}

FragTrap::FragTrap(std::string name)
{
	_hp = 100;
	_maxHp = 100;
	_ep = 100;
	_maxEp = 100;
	_level = 1;
	_name = name;
	_meleeAttDmg = 30;
	_rangedAttDmg = 20;
	_armorDmgReduc = 5;
	srand(time(NULL));
	std::cout << "Hi! I'm " << this->_name << "!" << std::endl;
}

FragTrap::FragTrap(FragTrap const & src)
{
	*this = src;
}

FragTrap::~FragTrap()
{
	std::cout << "I'm gonna take a nap" << std::endl;
}

FragTrap&
FragTrap::operator=(FragTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

void
FragTrap::rangedAttack(std::string const & target)
{
	ClapTrap::rangedAttack(target);
	std::cout << "Take that !" << std::endl;
}

void
FragTrap::meleeAttack(std::string const & target)
{
	ClapTrap::meleeAttack(target);
	std::cout << "Hyah !" << std::endl;
}

void
FragTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
	std::cout << "Arrg ! That hurt ! ... Why do I even feel pain ?!" << std::endl;
}

void
FragTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
	std::cout << "Health! Ooh what flavor is red?" << std::endl;
}

void
FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	std::string att[5] = {"Funzerker", "Meat Unicycle", "Shhhhhhh...trap", "BlightBot", "Gun Wizard"};
	std::string quote[5] = {"I'm a sexy dinosaur! Rawr.", "It's the only way to stop the voices", "I am cloaking...", "Mini-trap, pretend you're a Siren!", "You can call me Gundalf!"};
	int r = rand() % 5;

	if (this->_ep < 25)
	{
		std::cout << "* Not enough energy points to do that! *" << std::endl;
		return ;
	}
	std::cout << "* " << this->_name << " attacks " << target << " with "<< att[r] << " for 40 damage points *" << std::endl;
	std::cout << quote[r] << std::endl;
	this->_ep -= 25;
}
