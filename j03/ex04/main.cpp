// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 10:13:56 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 22:07:36 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"
#include "ScavTrap.hpp"
#include "NinjaTrap.hpp"
#include "SuperTrap.hpp"

void status(FragTrap & f)
{
	std::cout << f.getName() << " has " << f.getHp() << " hits points and " << f.getEp() << " energy points" << std::endl;
}

int main()
{
	SuperTrap f("pouet");
	NinjaTrap n("Ninj4");

	status(f);
	f.rangedAttack("Clap");
	f.meleeAttack("Clap");
	f.takeDamage(42);
	status(f);
	f.beRepaired(15);
	status(f);
	f.beRepaired(250);
	status(f);
	f.takeDamage(420);
	status(f);
	f.beRepaired(50);
	status(f);
	f.vaulthunter_dot_exe("Clap");
	status(f);
	f.vaulthunter_dot_exe("Clap");
	f.vaulthunter_dot_exe("Clap");
	f.vaulthunter_dot_exe("Clap");
	status(f);
	f.vaulthunter_dot_exe("Clap");
	f.ninjaShoebox(n);
	return (0);
}
