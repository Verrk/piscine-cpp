// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 10:07:23 by cpestour          #+#    #+#             //
//   Updated: 2015/06/18 11:33:42 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "FragTrap.hpp"

FragTrap::FragTrap():
	_hp(100),
	_maxHp(100),
	_ep(100),
	_maxEp(100),
	_level(1),
	_name("FR4G"),
	_meleeAttDmg(30),
	_rangedAttDmg(20),
	_armorDmgReduc(5)
{
	srand(time(NULL));
	std::cout << "* Boot sequence initialized *" << std::endl;
	std::cout << "Hi! I'm " << this->_name << "!" << std::endl;
}

FragTrap::FragTrap(std::string name):
	_hp(100),
	_maxHp(100),
	_ep(100),
	_maxEp(100),
	_level(1),
	_name(name),
	_meleeAttDmg(30),
	_rangedAttDmg(20),
	_armorDmgReduc(5)
{
	srand(time(NULL));
	std::cout << "* Boot sequence initialized *" << std::endl;
	std::cout << "Hi! I'm " << this->_name << "!" << std::endl;
}

FragTrap::FragTrap(FragTrap const & src)
{
	*this = src;
}

FragTrap::~FragTrap()
{
	std::cout << "* Destruction of " << this->_name << " in 3...2...1... *" << std::endl;
}

FragTrap&
FragTrap::operator=(FragTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

int
FragTrap::getHp() const
{
	return this->_hp;
}

int
FragTrap::getMaxHp() const
{
	return this->_maxHp;
}

int
FragTrap::getEp() const
{
	return this->_ep;
}

int
FragTrap::getMaxEp() const
{
	return this->_maxEp;
}

int
FragTrap::getLevel() const
{
	return this->_level;
}

std::string
FragTrap::getName() const
{
	return this->_name;
}

int
FragTrap::getMeleeAttDmg() const
{
	return this->_meleeAttDmg;
}

int
FragTrap::getRangedAttDmg() const
{
	return this->_rangedAttDmg;
}

int
FragTrap::getArmorDmgReduc() const
{
	return this->_armorDmgReduc;
}

void
FragTrap::rangedAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " at range, causing " << this->_rangedAttDmg << " points of damage ! *" << std::endl;
	std::cout << "Take that !" << std::endl;
}

void
FragTrap::meleeAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " in melee, causing " << this->_meleeAttDmg << " points of damage ! *" << std::endl;
	std::cout << "Hyah !" << std::endl;
}

void
FragTrap::takeDamage(unsigned int amount)
{
	unsigned int d;

	if (amount <= this->_armorDmgReduc)
	{
		std::cout << "My armor is too strong for you hehe" << std::endl;
		return ;
	}
	else
		d = amount - this->_armorDmgReduc;
	if (this->_hp <= d)
		this->_hp = 0;
	else
		this->_hp -= d;
	std::cout << "* " << this->_name << " takes " << d << " damages *" << std::endl;
	std::cout << "Arrg ! That hurt ! ... Why do I even feel pain ?!" << std::endl;
}

void
FragTrap::beRepaired(unsigned int amount)
{
	int d;

	if (this->_hp + amount >= this->_maxHp)
	{
		d = this->_maxHp - this->_hp;
		this->_hp = 100;
	}
	else
	{
		d = amount;
		this->_hp += amount;
	}
	std::cout << "* " << this->_name << " is healed by " << d << " hits points *" << std::endl;
	std::cout << "Health! Ooh what flavor is red?" << std::endl;
}

void
FragTrap::vaulthunter_dot_exe(std::string const & target)
{
	std::string att[5] = {"Funzerker", "Meat Unicycle", "Shhhhhhh...trap", "BlightBot", "Gun Wizard"};
	std::string quote[5] = {"I'm a sexy dinosaur! Rawr.", "It's the only way to stop the voices", "I am cloaking...", "Mini-trap, pretend you're a Siren!", "You can call me Gundalf!"};
	int r = rand() % 5;

	if (this->_ep < 25)
	{
		std::cout << "* Not enough energy points to do that! *" << std::endl;
		return ;
	}
	std::cout << "* " << this->_name << " attacks " << target << " with "<< att[r] << " for 40 damage points *" << std::endl;
	std::cout << quote[r] << std::endl;
	this->_ep -= 25;
}
