// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/25 14:20:43 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 10:04:14 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ScavTrap.hpp"

ScavTrap::ScavTrap()
{
	_hp = 100;
	_maxHp = 100;
	_ep = 50;
	_maxEp = 50;
	_level = 1;
	_name = "SC4V";
	_meleeAttDmg = 20;
	_rangedAttDmg = 15;
	_armorDmgReduc = 3;
	srand(time(NULL));
	std::cout << this->_name << " ready for some action !" << std::endl;
}

ScavTrap::ScavTrap(std::string name)
{
	_hp = 100;
	_maxHp = 100;
	_ep = 50;
	_maxEp = 50;
	_level = 1;
	_name = name;
	_meleeAttDmg = 20;
	_rangedAttDmg = 15;
	_armorDmgReduc = 3;
	srand(time(NULL));
	std::cout << this->_name << " ready for some action!" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const & src)
{
	*this = src;
}

ScavTrap::~ScavTrap()
{
	std::cout << this->_name << " out!" << std::endl;
}

ScavTrap&
ScavTrap::operator=(ScavTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

void
ScavTrap::rangedAttack(std::string const & target)
{
	ClapTrap::rangedAttack(target);
	std::cout << "WOW! I hit him !" << std::endl;
}

void
ScavTrap::meleeAttack(std::string const & target)
{
	ClapTrap::meleeAttack(target);
	std::cout << "Shwing !" << std::endl;
}

void
ScavTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
	std::cout << "My robotic flesh! AAHH!" << std::endl;
}

void
ScavTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
	std::cout << "Sweet life juice!" << std::endl;
}

void
ScavTrap::challengeNewcomer() const
{
	std::string challenges[5] = {"Me versus you! You versus me! Either way!", "I will prove to you my robotic superiority!", "Dance battle! Or, you know... regular battle.", "Man versus Machine!", "Care to have a friendly duel?"};
	std::cout << challenges[rand() % 5] << std::endl;
}
