// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ClapTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/25 14:58:09 by cpestour          #+#    #+#             //
//   Updated: 2015/10/25 15:22:22 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ClapTrap.hpp"

ClapTrap::ClapTrap()
{
	std::cout << "* Boot sequence initialized *" << std::endl;
}

ClapTrap::ClapTrap(ClapTrap const & src)
{
	*this = src;
}

ClapTrap::~ClapTrap()
{
	std::cout << "* Destruction in 3..2..1.. *" << std::endl;
}

ClapTrap&
ClapTrap::operator=(ClapTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

int
ClapTrap::getHp() const
{
	return this->_hp;
}

int
ClapTrap::getMaxHp() const
{
	return this->_maxHp;
}

int
ClapTrap::getEp() const
{
	return this->_ep;
}

int
ClapTrap::getMaxEp() const
{
	return this->_maxEp;
}

int
ClapTrap::getLevel() const
{
	return this->_level;
}

std::string
ClapTrap::getName() const
{
	return this->_name;
}

int
ClapTrap::getMeleeAttDmg() const
{
	return this->_meleeAttDmg;
}

int
ClapTrap::getRangedAttDmg() const
{
	return this->_rangedAttDmg;
}

int
ClapTrap::getArmorDmgReduc() const
{
	return this->_armorDmgReduc;
}

void
ClapTrap::rangedAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " at range, causing " << this->_rangedAttDmg << " points of damage ! *" << std::endl;
}

void
ClapTrap::meleeAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " in melee, causing " << this->_meleeAttDmg << " points of damage ! *" << std::endl;
}

void
ClapTrap::takeDamage(unsigned int amount)
{
	unsigned int d;

	if (amount <= this->_armorDmgReduc)
	{
		std::cout << "Can't touch this" << std::endl;
		return ;
	}
	else
		d = amount - this->_armorDmgReduc;
	if (this->_hp <= d)
		this->_hp = 0;
	else
		this->_hp -= d;
	std::cout << "* " << this->_name << " takes " << d << " damages *" << std::endl;
}

void
ClapTrap::beRepaired(unsigned int amount)
{
	int d;

	if (this->_hp + amount >= this->_maxHp)
	{
		d = this->_maxHp - this->_hp;
		this->_hp = 100;
	}
	else
	{
		d = amount;
		this->_hp += amount;
	}
	std::cout << "* " << this->_name << " is healed by " << d << " hits points *" << std::endl;
}

