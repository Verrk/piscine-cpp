// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   NinjaTrap.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 09:59:37 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 22:27:56 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "NinjaTrap.hpp"

NinjaTrap::NinjaTrap()
{
	_hp = 60;
	_maxHp = 60;
	_ep = 120;
	_maxEp = 120;
	_level = 1;
	_name = "NINJ4";
	_meleeAttDmg = 60;
	_rangedAttDmg = 5;
	_armorDmgReduc = 0;
	std::cout << this->_name << " is rising from the shadows" << std::endl;
}

NinjaTrap::NinjaTrap(std::string name)
{
	_hp = 60;
	_maxHp = 60;
	_ep = 120;
	_maxEp = 120;
	_level = 1;
	_name = name;
	_meleeAttDmg = 60;
	_rangedAttDmg = 5;
	_armorDmgReduc = 0;
	std::cout << this->_name << " is rising from the shadows" << std::endl;
}

NinjaTrap::NinjaTrap(NinjaTrap const & src)
{
	*this = src;
}

NinjaTrap::~NinjaTrap()
{
	std::cout << this->_name << " diseppeared in darkness" << std::endl;
}

NinjaTrap&
NinjaTrap::operator=(NinjaTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

void
NinjaTrap::rangedAttack(std::string const & target)
{
	ClapTrap::rangedAttack(target);
	std::cout << "I sould get closer!" << std::endl;
}

void
NinjaTrap::meleeAttack(std::string const & target)
{
	ClapTrap::meleeAttack(target);
	std::cout << "I'm too fast for you!" << std::endl;
}

void
NinjaTrap::takeDamage(unsigned int amount)
{
	ClapTrap::takeDamage(amount);
	std::cout << "Ow hohoho, that hurts! Yipes!" << std::endl;
}

void
NinjaTrap::beRepaired(unsigned int amount)
{
	ClapTrap::beRepaired(amount);
	std::cout << "I found health" << std::endl;
}

void
NinjaTrap::ninjaShoebox(FragTrap const & target)
{
	std::cout << "Frag spotted right ahead ! His name is " << target.getName() << std::endl;
}

void
NinjaTrap::ninjaShoebox(ScavTrap const & target)
{
	std::cout << "Scav spotted right ahead ! His name is " << target.getName() << std::endl;
}

void
NinjaTrap::ninjaShoebox(NinjaTrap const & target)
{
	std::cout << "Ninja spotted right ahead ! His name is " << target.getName() << std::endl;
}
