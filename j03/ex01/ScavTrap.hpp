// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/25 14:20:51 by cpestour          #+#    #+#             //
//   Updated: 2015/10/25 14:32:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SCAVTRAP_HPP
# define SCAVTRAP_HPP

# include <iostream>
# include <cstdlib>
# include <ctime>

class ScavTrap
{
public:
	ScavTrap();
	ScavTrap(std::string name);
	ScavTrap(ScavTrap const & src);
	~ScavTrap();

	ScavTrap& operator=(ScavTrap const & rhs);

	int getHp() const;
	int getMaxHp() const;
	int getEp() const;
	int getMaxEp() const;
	int getLevel() const;
	std::string getName() const;
	int getMeleeAttDmg() const;
	int getRangedAttDmg() const;
	int getArmorDmgReduc() const;

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);
	void challengeNewcomer() const;

private:
	unsigned int _hp;
	unsigned int _maxHp;
	unsigned int _ep;
	unsigned int _maxEp;
	unsigned int _level;
	std::string _name;
	unsigned int _meleeAttDmg;
	unsigned int _rangedAttDmg;
	unsigned int _armorDmgReduc;
};

#endif
