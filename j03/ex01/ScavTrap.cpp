// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ScavTrap.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/25 14:20:43 by cpestour          #+#    #+#             //
//   Updated: 2015/10/25 14:48:20 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ScavTrap.hpp"

ScavTrap::ScavTrap():
	_hp(100),
	_maxHp(100),
	_ep(50),
	_maxEp(50),
	_level(1),
	_name("SC4V"),
	_meleeAttDmg(20),
	_rangedAttDmg(15),
	_armorDmgReduc(3)
{
	srand(time(NULL));
	std::cout << "* Boot sequence initialized *" << std::endl;
	std::cout << this->_name << " ready for some action !" << std::endl;
}

ScavTrap::ScavTrap(std::string name):
	_hp(100),
	_maxHp(100),
	_ep(50),
	_maxEp(50),
	_level(1),
	_name(name),
	_meleeAttDmg(20),
	_rangedAttDmg(15),
	_armorDmgReduc(3)
{
	srand(time(NULL));
	std::cout << "* Boot sequence initialized *" << std::endl;
	std::cout << this->_name << " ready for some action!" << std::endl;
}

ScavTrap::ScavTrap(ScavTrap const & src)
{
	*this = src;
}

ScavTrap::~ScavTrap()
{
	std::cout << "* Destruction of " << this->_name << " in 3...2...1... *" << std::endl;
}

ScavTrap&
ScavTrap::operator=(ScavTrap const & rhs)
{
	this->_hp = rhs.getHp();
	this->_maxHp = rhs.getMaxHp();
	this->_ep = rhs.getEp();
	this->_maxEp = rhs.getMaxEp();
	this->_level = rhs.getLevel();
	this->_name = rhs.getName();
	this->_meleeAttDmg = rhs.getMeleeAttDmg();
	this->_rangedAttDmg = rhs.getRangedAttDmg();
	this->_armorDmgReduc = rhs.getArmorDmgReduc();
	return (*this);
}

int
ScavTrap::getHp() const
{
	return this->_hp;
}

int
ScavTrap::getMaxHp() const
{
	return this->_maxHp;
}

int
ScavTrap::getEp() const
{
	return this->_ep;
}

int
ScavTrap::getMaxEp() const
{
	return this->_maxEp;
}

int
ScavTrap::getLevel() const
{
	return this->_level;
}

std::string
ScavTrap::getName() const
{
	return this->_name;
}

int
ScavTrap::getMeleeAttDmg() const
{
	return this->_meleeAttDmg;
}

int
ScavTrap::getRangedAttDmg() const
{
	return this->_rangedAttDmg;
}

int
ScavTrap::getArmorDmgReduc() const
{
	return this->_armorDmgReduc;
}

void
ScavTrap::rangedAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " at range, causing " << this->_rangedAttDmg << " points of damage ! *" << std::endl;
	std::cout << "WOW! I hit him !" << std::endl;
}

void
ScavTrap::meleeAttack(std::string const & target)
{
	std::cout << "* " << this->_name << " attacks " << target << " in melee, causing " << this->_meleeAttDmg << " points of damage ! *" << std::endl;
	std::cout << "Shwing !" << std::endl;
}

void
ScavTrap::takeDamage(unsigned int amount)
{
	unsigned int d;

	if (amount <= this->_armorDmgReduc)
	{
		std::cout << "Can't touch this" << std::endl;
		return ;
	}
	else
		d = amount - this->_armorDmgReduc;
	if (this->_hp <= d)
		this->_hp = 0;
	else
		this->_hp -= d;
	std::cout << "* " << this->_name << " takes " << d << " damages *" << std::endl;
	std::cout << "My robotic flesh! AAHH!" << std::endl;
}

void
ScavTrap::beRepaired(unsigned int amount)
{
	int d;

	if (this->_hp + amount >= this->_maxHp)
	{
		d = this->_maxHp - this->_hp;
		this->_hp = 100;
	}
	else
	{
		d = amount;
		this->_hp += amount;
	}
	std::cout << "* " << this->_name << " is healed by " << d << " hits points *" << std::endl;
	std::cout << "Sweet life juice!" << std::endl;
}

void
ScavTrap::challengeNewcomer() const
{
	std::string challenges[5] = {"Me versus you! You versus me! Either way!", "I will prove to you my robotic superiority!", "Dance battle! Or, you know... regular battle.", "Man versus Machine!", "Care to have a friendly duel?"};
	std::cout << challenges[rand() % 5] << std::endl;
}
