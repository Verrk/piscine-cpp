// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   FragTrap.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/18 09:52:30 by cpestour          #+#    #+#             //
//   Updated: 2015/06/18 11:19:50 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include <iostream>
# include <cstdlib>
# include <ctime>

class FragTrap
{
public:
	FragTrap();
	FragTrap(std::string name);
	FragTrap(FragTrap const & src);
	~FragTrap();

	FragTrap& operator=(FragTrap const & rhs);

	int getHp() const;
	int getMaxHp() const;
	int getEp() const;
	int getMaxEp() const;
	int getLevel() const;
	std::string getName() const;
	int getMeleeAttDmg() const;
	int getRangedAttDmg() const;
	int getArmorDmgReduc() const;

	void rangedAttack(std::string const & target);
	void meleeAttack(std::string const & target);
	void takeDamage(unsigned int amount);
	void beRepaired(unsigned int amount);
	void vaulthunter_dot_exe(std::string const & target);

private:
	unsigned int _hp;
	unsigned int _maxHp;
	unsigned int _ep;
	unsigned int _maxEp;
	unsigned int _level;
	std::string _name;
	unsigned int _meleeAttDmg;
	unsigned int _rangedAttDmg;
	unsigned int _armorDmgReduc;
};

#endif
