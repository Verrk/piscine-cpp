// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 22:05:25 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 22:34:45 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Form.hpp"

Form::Form():
	_name("form"),
	_signed(false),
	_signGrade(150),
	_execGrade(150)
{}

Form::Form(std::string name, int signGrade, int execGrade):
	_name(name),
	_signed(false),
	_signGrade(signGrade),
	_execGrade(execGrade)
{
	if (signGrade > 150 || execGrade > 150)
		throw Form::GradeTooLowException();
	if (signGrade < 1 || execGrade < 1)
		throw Form::GradeTooHighException();
}

Form::Form(Form const & src):
	_name(src.getName()),
	_signGrade(src.getSignGrade()),
	_execGrade(src.getExecGrade())
{
	*this = src;
}

Form &
Form::operator=(Form const & rhs)
{
	this->_signed = rhs.getSigned();
	return *this;
}

Form::~Form() {}

std::string const &
Form::getName() const
{
	return _name;
}

bool
Form::getSigned() const
{
	return _signed;
}

int
Form::getSignGrade() const
{
	return _signGrade;
}

int
Form::getExecGrade() const
{
	return _execGrade;
}

void
Form::beSigned(Bureaucrat const & b)
{
	if (b.getGrade() > _signGrade)
		throw Form::GradeTooLowException();
	_signed = true;
}

const char *
Form::GradeTooHighException::what() const throw()
{
	return "grade too high!";
}

const char *
Form::GradeTooLowException::what() const throw()
{
	return "grade too low!";
}

std::ostream & operator<<(std::ostream & o, Form const & rhs)
{
	o << "Form " << rhs.getName() << ", ";
	if (rhs.getSigned() == false)
		o << "not ";
	o << "signed. Grade to sign: " << rhs.getSignGrade();
	o << ". Grade to execute: " << rhs.getExecGrade();
	return o;
}
