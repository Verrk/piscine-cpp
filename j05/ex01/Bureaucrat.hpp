// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 17:26:52 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 22:30:59 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP

# include <iostream>
# include <exception>

class Form;

class Bureaucrat
{
public:
	Bureaucrat();
	Bureaucrat(std::string const & name, int grade);
	Bureaucrat(Bureaucrat const & src);
	Bureaucrat & operator=(Bureaucrat const & rhs);
	~Bureaucrat();

	std::string const & getName() const;
	int getGrade() const;
	void incGrade();
	void decGrade();

	void signForm(Form & f);

	class GradeTooHighException: public std::exception
	{
	public:
		const char * what() const throw();
	};

	class GradeTooLowException: public std::exception
	{
	public:
		const char * what() const throw();
	};

private:
	const std::string _name;
	int _grade;
};

std::ostream & operator<<(std::ostream & o, Bureaucrat const & rhs);

# include "Form.hpp"

#endif
