// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Form.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 21:57:58 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 22:27:53 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef FORM_HPP
# define FORM_HPP

# include <iostream>
# include <exception>

class Bureaucrat;

class Form
{
public:
	Form();
	Form(std::string name, int signGrade, int execGrade);
	Form(Form const & src);
	Form & operator=(Form const & rhs);
	~Form();

	std::string const & getName() const;
	bool getSigned() const;
	int getSignGrade() const;
	int getExecGrade() const;

	void beSigned(Bureaucrat const & b);

	class GradeTooHighException: public std::exception
	{
	public:
		const char * what() const throw();
	};

	class GradeTooLowException: public std::exception
	{
	public:
		const char * what() const throw();
	};

private:
	const std::string _name;
	bool _signed;
	const int _signGrade;
	const int _execGrade;
};

std::ostream & operator<<(std::ostream & o, Form const & rhs);

# include "Bureaucrat.hpp"

#endif
