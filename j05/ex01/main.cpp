// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 18:00:49 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 22:36:07 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <exception>

int main()
{
	try
	{
//		Bureaucrat a("Bob", 200);
		Bureaucrat b("Jim", 3);
		Bureaucrat c("Jim", 148);
//		Form f1("asdf", 140, 0);
		Form f("f4221", 42, 21);

		std::cout << b << std::endl << c << std::endl;
		std::cout << f << std::endl;

		b.signForm(f);
		std::cout << f << std::endl;
		c.signForm(f);
	}
	catch (std::exception & e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
}
