// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 18:00:49 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 18:20:36 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"
#include <exception>

int main()
{
	try
	{
//		Bureaucrat a("Bob", 200);
		Bureaucrat b("Jim", 3);
		Bureaucrat c("Jim", 148);

		std::cout << b << std::endl << c << std::endl;

		b.incGrade();
		b.incGrade();
		std::cout << "grade: " << b.getGrade() << std::endl;
//		b.incGrade();

		c.decGrade();
		c.decGrade();
		std::cout << "grade: " << c.getGrade() << std::endl;
//		c.decGrade();
	}
	catch (std::exception & e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
}
