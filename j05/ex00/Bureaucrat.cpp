// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Bureaucrat.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 17:39:32 by cpestour          #+#    #+#             //
//   Updated: 2015/11/05 18:17:38 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Bureaucrat.hpp"

Bureaucrat::Bureaucrat():
	_name("random"),
	_grade(150)
{}

Bureaucrat::Bureaucrat(std::string const & name, int grade):
	_name(name)
{
	if (grade < 1)
		throw GradeTooHighException();
	if (grade > 150)
		throw GradeTooLowException();
	_grade = grade;
}

Bureaucrat::Bureaucrat(Bureaucrat const & src):
	_name(src.getName())
{
	*this = src;
}

Bureaucrat &
Bureaucrat::operator=(Bureaucrat const & rhs)
{
	this->_grade = rhs.getGrade();
	return *this;
}

Bureaucrat::~Bureaucrat() {}

std::string const &
Bureaucrat::getName() const
{
	return _name;
}

int
Bureaucrat::getGrade() const
{
	return _grade;
}

void
Bureaucrat::incGrade()
{
	if (this->_grade == 1)
		throw GradeTooHighException();
	this->_grade--;
}

void
Bureaucrat::decGrade()
{
	if (this->_grade == 150)
		throw GradeTooLowException();
	this->_grade++;
}

const char *
Bureaucrat::GradeTooHighException::what() const throw()
{
	return "Grade too high!";
}

const char *
Bureaucrat::GradeTooLowException::what() const throw()
{
	return "Grade too low!";
}

std::ostream & operator<<(std::ostream & o, Bureaucrat const & rhs)
{
	o << rhs.getName() << " bureaucrat grade " << rhs.getGrade();
	return o;
}
