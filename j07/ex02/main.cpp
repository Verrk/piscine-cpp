// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 20:20:44 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 20:32:11 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Array.hpp"
#include <iostream>

int main()
{
	Array<int> a(10);

	try
	{
		for (int i = 0; i < 10; i++)
			a[i] = i;
		for (int i = 0; i < 10; i++)
			std::cout << a[i];
		std::cout << std::endl;
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
	}
}
