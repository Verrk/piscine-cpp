// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Array.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 20:09:21 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 21:31:20 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ARRAY_HPP
# define ARRAY_HPP

# include <iostream>
# include <exception>

template<typename T>
class Array
{
public:
	Array():
		_n(0)
		{
			_tab = new T[0];
		}

	Array(unsigned int n):
		_n(n)
		{
			_tab = new T[n];
		}

	Array(Array const & src)
		{
			*this = src;
		}

	Array & operator=(Array const & rhs)
		{
			delete [] this->_tab;
			this->_n = rhs.size();
			this->_tab = new T[_n];
			for (int i = 0; i < _n; i++)
				this->_tab[i] = rhs[i];
			return *this;
		}

	~Array()
		{
			delete [] _tab;
		}

	T & operator[](unsigned int n)
		{
			if (n >= _n)
				throw std::exception();
			else
				return _tab[n];
		}

	unsigned int size() const
		{
			return _n;
		}

private:
	unsigned int _n;
	T * _tab;
};

#endif
