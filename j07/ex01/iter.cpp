// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   iter.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 20:02:38 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 20:07:51 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

void printInt(int i)
{
	std::cout << i;
}

void printChar(char c)
{
	std::cout << c;
}

template<typename T>
void iter(T * tab, size_t size, void (*f)(T))
{
	for (size_t i = 0; i < size; i++)
		f(tab[i]);
}

int main()
{
	int tab[10];
	char ctab[5];

	for (int i = 0; i < 10; i++)
		tab[i] = i;
	for (int i = 0; i < 5; i++)
		ctab[i] = i + 97;
	iter(tab, 10, printInt);
	std::cout << std::endl;
	iter(ctab, 5, printChar);
	std::cout << std::endl;
}
