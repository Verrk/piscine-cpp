// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   whatever.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/09 19:51:01 by cpestour          #+#    #+#             //
//   Updated: 2015/11/09 19:59:52 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

template<typename T>
void swap(T & a, T & b)
{
	T tmp(a);
	a = b;
	b = tmp;
}

template<typename T>
T & min(T & a, T & b)
{
	if (a < b)
		return a;
	else
		return b;
}

template<typename T>
T & max(T & a, T & b)
{
	if (a > b)
		return a;
	else
		return b;
}

int main()
{
	int a = 2;
	int b = 3;

	std::cout << "a: " << a << "; b: " << b << std::endl;
	::swap(a, b);
	std::cout << "swap: a: " << a << "; b: " << b << std::endl;
	std::cout << "min: " << ::min(a, b) << std::endl;
	std::cout << "max: " << ::max(a, b) << std::endl;

	std::string s1("chaine1");
	std::string s2("chaine2");

	std::cout << "s1: " << s1 << "; s2: " << s2 << std::endl;
	::swap(s1, s2);
	std::cout << "swap: s1: " << s1 << "; s2: " << s2 << std::endl;
	std::cout << "min: " << ::min(s1, s2) << std::endl;
	std::cout << "max: " << ::max(s1, s2) << std::endl;
}
