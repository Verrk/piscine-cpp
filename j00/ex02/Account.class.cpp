// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Account.class.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 13:26:39 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 10:59:16 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Account.class.hpp"
#include <iostream>
#include <ctime>

int Account::_nbAccounts = 0;
int Account::_totalAmount = 0;
int Account::_totalNbDeposits = 0;
int Account::_totalNbWithdrawals = 0;

int
Account::getNbAccounts()
{
	return (Account::_nbAccounts);
}

int
Account::getTotalAmount()
{
	return (Account::_totalAmount);
}

int
Account::getNbDeposits()
{
	return (Account::_totalNbDeposits);
}

int
Account::getNbWithdrawals()
{
	return (Account::_totalNbWithdrawals);
}

void
Account::displayAccountsInfos()
{
	Account::_displayTimestamp();
	std::cout << "accounts:" << Account::getNbAccounts();
	std::cout << ";total:" << Account::getTotalAmount();
	std::cout << ";deposits:" << Account::getNbDeposits();
	std::cout << ";withdrawals:" << Account::getNbWithdrawals();
	std::cout << std::endl;
}

Account::Account(int initial_deposit):
	_accountIndex(Account::_nbAccounts),
	_amount(initial_deposit),
	_nbDeposits(0),
	_nbWithdrawals(0),
	_nbCheckAmountCalls(0)
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex;
	std::cout << ";amount:" << this->_amount;
	std::cout << ";created" << std::endl;
	Account::_nbAccounts++;
	Account::_totalAmount += initial_deposit;
}

Account::~Account()
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex;
	std::cout << ";amount:" << this->_amount;
	std::cout << ";closed" << std::endl;
	Account::_nbAccounts--;
	Account::_totalAmount -= this->_amount;
}

void
Account::makeDeposit(int deposit)
{
	int oldAmount;

	if (deposit > 0)
	{
		oldAmount = this->_amount;
		this->_amount += deposit;
		Account::_totalAmount += deposit;
		this->_nbDeposits++;
		Account::_totalNbDeposits++;
		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex;
		std::cout << ";p_amount:" << oldAmount;
		std::cout << ";deposit:" << deposit;
		std::cout << ";amount:" << this->_amount;
		std::cout << ";nb_deposits:" << this->_nbDeposits;
		std::cout << std::endl;
	}
	else
		std::cout << "Must be a positive integer" << std::endl;
}

bool
Account::makeWithdrawal(int withdrawal)
{
	int oldAmount;

	if (withdrawal > 0)
	{
		oldAmount = this->_amount;
		this->_amount -= withdrawal;
		Account::_displayTimestamp();
		std::cout << "index:" << this->_accountIndex;
		std::cout << ";p_amount:" << oldAmount;
		std::cout << ";withdrawal:";
		if (!checkAmount())
		{
			std::cout << "refused" << std::endl;
			this->_amount = oldAmount;
			return (false);
		}
		else
		{
			this->_nbWithdrawals++;
			Account::_totalNbWithdrawals++;
			Account::_totalAmount -= withdrawal;
			std::cout << withdrawal;
			std::cout << ";amount:" << this->_amount;
			std::cout << ";nb_withdrawals:" << this->_nbWithdrawals;
			std::cout << std::endl;
			return (true);
		}
	}
	else
	{
		std::cout << "Must be a positive integer" << std::endl;
		return (false);
	}
}

int
Account::checkAmount() const
{
	this->_nbCheckAmountCalls++;
	if (this->_amount < 0)
		return (0);
	else
		return (1);
}

void
Account::displayStatus() const
{
	Account::_displayTimestamp();
	std::cout << "index:" << this->_accountIndex;
	std::cout << ";amount:" << this->_amount;
	std::cout << ";deposits:" << this->_nbDeposits;
	std::cout << ";withdrawals:" << this->_nbWithdrawals;
	std::cout << std::endl;
}

void
Account::_displayTimestamp()
{
	char str[19];
	time_t t;

	time(&t);
	strftime(str, 19, "[%Y%m%d_%H%M%S] ", localtime(&t));
	std::cout << str;
}
