// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   megaphone.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 09:16:25 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 10:34:05 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

void	print_upper(char *str)
{
	int	i;

	for (i = 0; str[i]; i++)
	{
		if (str[i] >= 'a' && str[i] <= 'z')
			str[i] -= 32;
		std::cout << str[i];
	}
}

int		main(int ac, char **av)
{
	int	i;

	if (ac == 1)
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *" << std::endl;
	else
	{
		for(i = 1; i < ac; i++)
			print_upper(av[i]);
		std::cout << std::endl;
	}
	return (0);
}
