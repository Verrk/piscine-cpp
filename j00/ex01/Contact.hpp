// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Contact.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:16:32 by cpestour          #+#    #+#             //
//   Updated: 2015/06/15 18:28:09 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CONTACT_HPP
# define CONTACT_HPP

# include <iostream>
# include <iomanip>
# include <string>

class Contact
{
public:
	Contact();
	~Contact();
	void addContact();
	void displayColumn();
	void displayInfo();
	static int getNbContact();

private:
	std::string _firstname;
	std::string _lastname;
	std::string _nickname;
	std::string _login;
	std::string _address;
	std::string _email;
	std::string _phonenumber;
	std::string _birthdate;
	std::string _favoriteMeal;
	std::string _underwearColor;
	std::string _secret;
	int			_index;
	static int	_nbContact;
};

#endif
