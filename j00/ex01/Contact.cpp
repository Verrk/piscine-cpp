// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Contact.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:24:21 by cpestour          #+#    #+#             //
//   Updated: 2015/06/15 18:28:40 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Contact.hpp"

int Contact::_nbContact = 0;

Contact::Contact() {}

Contact::~Contact() {}

void
Contact::addContact()
{
	std::cout << "First name ? ";
	std::getline(std::cin, this->_firstname);
	std::cout << "Last name ? ";
	std::getline(std::cin, this->_lastname);
	std::cout << "Nickname ? ";
	std::getline(std::cin, this->_nickname);
	std::cout << "Login ? ";
	std::getline(std::cin, this->_login);
	std::cout << "Address ? ";
	std::getline(std::cin, this->_address);
	std::cout << "Email ? ";
	std::getline(std::cin, this->_email);
	std::cout << "Phone number ? ";
	std::getline(std::cin, this->_phonenumber);
	std::cout << "Birthdate ? ";
	std::getline(std::cin, this->_birthdate);
	std::cout << "Favorite meal ? ";
	std::getline(std::cin, this->_favoriteMeal);
	std::cout << "Underwear color ? ";
	std::getline(std::cin, this->_underwearColor);
	std::cout << "Darkest secret ? ";
	std::getline(std::cin, this->_secret);
	this->_index = Contact::_nbContact;
	Contact::_nbContact++;
}

void
Contact::displayColumn()
{
	std::cout << std::setw(10) << this->_index;
	std::cout << "|";
	if (this->_firstname.length() > 10)
		std::cout << std::setw(9) << this->_firstname.substr(0, 9) << ".";
	else
		std::cout << std::setw(10) << this->_firstname;
	std::cout << "|";
	if (this->_lastname.length() > 10)
		std::cout << std::setw(9) << this->_lastname.substr(0, 9) << ".";
	else
		std::cout << std::setw(10) << this->_lastname;
	std::cout << "|";
	if (this->_nickname.length() > 10)
		std::cout << std::setw(9) << this->_nickname.substr(0, 9) << ".";
	else
		std::cout << std::setw(10) << this->_nickname;
	std::cout << "|" << std::endl;
}

void
Contact::displayInfo()
{
	std::cout << "First name: " << this->_firstname << std::endl;
	std::cout << "Last name: " << this->_lastname << std::endl;
	std::cout << "Nickname: " << this->_nickname << std::endl;
	std::cout << "Login: " << this->_login << std::endl;
	std::cout << "Address: " << this->_address << std::endl;
	std::cout << "Email: " << this->_email << std::endl;
	std::cout << "Phone Number: " << this->_phonenumber << std::endl;
	std::cout << "Birthdate: " << this->_birthdate << std::endl;
	std::cout << "Favorite meal: " << this->_favoriteMeal << std::endl;
	std::cout << "Underwear color: " << this->_underwearColor << std::endl;
	std::cout << "Darkest secret: " << this->_secret << std::endl;
}

int
Contact::getNbContact()
{
	return Contact::_nbContact;
}
