// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/15 11:35:16 by cpestour          #+#    #+#             //
//   Updated: 2015/10/29 15:59:21 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <cstdlib>
#include "Contact.hpp"

int main()
{
	Contact contactList[8];
	std::string buf;
	int i;
	std::string id;
	int iid;
	bool b;

	std::cout << "Welcome to your phonebook !" << std::endl;
	std::cout << "Commands are 'ADD' 'SEARCH' and 'EXIT'" << std::endl;
	std::cout << "$> ";
	std::getline(std::cin, buf);
	while (buf.compare("EXIT") && !std::cin.eof())
	{
		b = false;
		if (!buf.compare("ADD"))
		{
			if (Contact::getNbContact() >= 8)
				std::cout << "Too many contact in the phonebook !" << std::endl;
			else
				contactList[Contact::getNbContact()].addContact();
		}
		else if (!buf.compare("SEARCH"))
		{
			if (Contact::getNbContact() > 0)
			{
				std::cout << "     Index|first name| last name|  nickname|" << std::endl;
				for (i = 0; i < Contact::getNbContact(); i++)
					contactList[i].displayColumn();
				while (b == false)
				{
					std::cout << "Choose an index: ";
					std::getline(std::cin, id);
					if (isdigit(id[0]))
					{
						iid = atoi(id.c_str());
						if (iid >= 0 && iid < Contact::getNbContact())
						{
							contactList[iid].displayInfo();
							b = true;
						}
						else
							std::cout << "Index not in the contact list" << std::endl;
					}
					else
						std::cout << "Wrong input" << std::endl;
				}
			}
			else
				std::cout << "Phonebook empty" << std::endl;
		}
		else if (buf.compare(""))
			std::cout << "Wrong command" << std::endl;
		std::cout << "$> ";
		std::getline(std::cin, buf);
	}
	std::cout << "GoodBye!" << std::endl;
	return (0);
}
