// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 09:36:07 by cpestour          #+#    #+#             //
//   Updated: 2015/11/06 03:13:23 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Fixed.hpp"

Fixed::Fixed():
	_bits(8),
	_raw(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(int const i):
	_bits(8)
{
	std::cout << "Int constructor called" << std::endl;
	this->_raw = i << this->_bits;
}

Fixed::Fixed(float const f):
	_bits(8)
{
	std::cout << "Float constructor called" << std::endl;
	this->_raw = roundf(f * pow(2, this->_bits));
}

Fixed::Fixed(Fixed const & src):
	_bits(8)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = src;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called" << std::endl;
}

Fixed&
Fixed::operator=(Fixed const & rhs)
{
	std::cout << "Assignation operator called" << std::endl;
	this->_raw = rhs.getRawBits();
	return (*this);
}

int
Fixed::getRawBits() const
{
	return (this->_raw);
}

void
Fixed::setRawBits(int const raw)
{
	this->_raw = raw;
}

float
Fixed::toFloat() const
{
	return ((float)this->_raw / pow(2, this->_bits));
}

int
Fixed::toInt() const
{
	return (this->_raw >> this->_bits);
}

std::ostream &operator<<(std::ostream& o, Fixed const & rhs)
{
	o << rhs.toFloat();
	return (o);
}
