// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Parser.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 23:36:21 by cpestour          #+#    #+#             //
//   Updated: 2015/11/06 02:57:21 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Parser.hpp"

Parser::Parser() {}

Parser::Parser(std::string expr):
	ss(expr),
	_expr(expr)
{}

Parser::Parser(Parser const & src):
	ss(src.getExpr()),
	_expr(src.getExpr())
{
	*this = src;
}

Parser &
Parser::operator=(Parser const & rhs)
{
	(void)rhs;
	return *this;
}

Parser::~Parser() {}

std::string
Parser::getExpr() const
{
	return _expr;
}

Fixed
Parser::parse()
{
	return expr();
}

Fixed
Parser::expr()
{
	Fixed f1 = term();
	skipSpace();
	while (ss.peek() == '+' || ss.peek() == '-')
	{
		char op;
		ss >> op;
		Fixed f2 = term();
		if (op == '+')
			f1 = f1 + f2;
		else
			f1 = f1 - f2;
	}
	return f1;
}

Fixed
Parser::term()
{
	Fixed f1 = factor();
	skipSpace();
	while (ss.peek() == '*' || ss.peek() == '/')
	{
		char op;
		ss >> op;
		Fixed f2 = factor();
		if (op == '*')
			f1 = f1 * f2;
		else
			f1 = f1 / f2;
	}
	return f1;
}

Fixed
Parser::factor()
{
	skipSpace();
	if (isdigit(ss.peek()))
	{
		float num;
		ss >> num;
		Fixed f(num);
		return f;
	}
	else if (ss.peek() == '(')
	{
		char paren;
		ss >> paren;
		Fixed f = expr();
		if (ss.peek() != ')')
		{
			std::cout << "Error: bracket mismatch" << std::endl;
			exit(EXIT_FAILURE);
		}
		ss >> paren;
		if (ss.peek() == ')')
		{
			std::cout << "Error: bracket mismatch" << std::endl;
			exit(EXIT_FAILURE);
		}
		return f;
	}
	else
	{
		std::cout << "Error: bad token" << std::endl;
		exit(EXIT_FAILURE);
	}
}

void
Parser::skipSpace()
{
	char dump;

	while (isspace(ss.peek()))
		ss >> std::noskipws >> dump;
}
