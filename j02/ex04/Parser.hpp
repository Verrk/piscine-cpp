// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Parser.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 23:33:05 by cpestour          #+#    #+#             //
//   Updated: 2015/11/06 02:57:26 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PARSER_HPP
# define PARSER_HPP

# include <iostream>
# include <sstream>
# include <cstdlib>
# include "Fixed.hpp"

class Parser
{
public:
	Parser();
	Parser(std::string expr);
	Parser(Parser const & src);
	Parser & operator=(Parser const & rhs);
	~Parser();

	std::string getExpr() const;

	Fixed parse();
	Fixed expr();
	Fixed term();
	Fixed factor();
	void skipSpace();

private:
	std::stringstream ss;
	std::string _expr;
};

#endif
