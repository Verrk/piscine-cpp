// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/05 23:26:43 by cpestour          #+#    #+#             //
//   Updated: 2015/11/06 02:58:16 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Parser.hpp"
#include "Fixed.hpp"

int main(int ac, char **av)
{
	Fixed f;

	if (ac > 1)
	{
		for (int i = 1; i < ac; i++)
		{
			Parser p(av[i]);
			f = p.parse();
			std::cout << f << std::endl;
		}
	}
	else
		std::cout << "./eval_expr [expr]" << std::endl;
}
