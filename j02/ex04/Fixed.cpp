// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Fixed.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/17 09:36:07 by cpestour          #+#    #+#             //
//   Updated: 2015/11/06 03:12:47 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Fixed.hpp"

Fixed::Fixed():
	_bits(8),
	_raw(0)
{}

Fixed::Fixed(int const i):
	_bits(8)
{
	this->_raw = i << Fixed::_bits;
}

Fixed::Fixed(float const f):
	_bits(8)
{
	this->_raw = roundf(f * pow(2, Fixed::_bits));
}

Fixed::Fixed(Fixed const & src):
	_bits(8)
{
	*this = src;
}

Fixed::~Fixed() {}

Fixed&
Fixed::operator=(Fixed const & rhs)
{
	this->_raw = rhs.getRawBits();
	return (*this);
}

bool
Fixed::operator>(Fixed const & rhs) const
{
	return (this->toFloat() > rhs.toFloat());
}

bool
Fixed::operator<(Fixed const & rhs) const
{
	return (this->toFloat() < rhs.toFloat());
}

bool
Fixed::operator>=(Fixed const & rhs) const
{
	return (this->toFloat() >= rhs.toFloat());
}


bool
Fixed::operator<=(Fixed const & rhs) const
{
	return (this->toFloat() <= rhs.toFloat());
}

bool
Fixed::operator==(Fixed const & rhs) const
{
	return (this->toFloat() == rhs.toFloat());
}

bool
Fixed::operator!=(Fixed const & rhs) const
{
	return (this->toFloat() != rhs.toFloat());
}

Fixed
Fixed::operator+(Fixed const & rhs) const
{
	return (Fixed(this->toFloat() + rhs.toFloat()));
}

Fixed
Fixed::operator-(Fixed const & rhs) const
{
	return (Fixed(this->toFloat() - rhs.toFloat()));
}

Fixed
Fixed::operator*(Fixed const & rhs) const
{
	return (Fixed(this->toFloat() * rhs.toFloat()));
}

Fixed
Fixed::operator/(Fixed const & rhs) const
{
	return (Fixed(this->toFloat() / rhs.toFloat()));
}

Fixed&
Fixed::operator++()
{
	this->_raw++;
	return (*this);
}

Fixed
Fixed::operator++(int)
{
	Fixed tmp(*this);
	++(*this);
	return (tmp);
}

Fixed&
Fixed::operator--()
{
	this->_raw--;
	return (*this);
}

Fixed
Fixed::operator--(int)
{
	Fixed tmp(*this);
	--(*this);
	return (tmp);
}

int
Fixed::getRawBits() const
{
	return (this->_raw);
}

void
Fixed::setRawBits(int const raw)
{
	this->_raw = raw;
}

float
Fixed::toFloat() const
{
	return ((float)this->_raw / pow(2, this->_bits));
}

int
Fixed::toInt() const
{
	return (this->_raw >> this->_bits);
}

Fixed const&
Fixed::min(Fixed const & a, Fixed const & b)
{
	if (a < b)
		return (a);
	else
		return (b);
}

Fixed const&
Fixed::max(Fixed const & a, Fixed const & b)
{
	if (a > b)
		return (a);
	else
		return (b);
}

std::ostream &operator<<(std::ostream& o, Fixed const & rhs)
{
	o << rhs.toFloat();
	return (o);
}
