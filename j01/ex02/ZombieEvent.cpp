// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieEvent.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:40:58 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 11:11:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieEvent.hpp"

ZombieEvent::ZombieEvent():
	_type("Unknown")
{}

ZombieEvent::~ZombieEvent() {}

void
ZombieEvent::setZombieType(std::string type)
{
	this->_type = type;
}

Zombie *
ZombieEvent::newZombie(std::string name) const
{
	return (new Zombie(name, this->_type));
}
