// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:45:27 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 11:09:50 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieEvent.hpp"
#include "Zombie.hpp"
#include <cstdlib>
#include <ctime>

void randomChump()
{
	ZombieEvent e;
	std::string tab[10] = {"Roger", "Clement", "Julie", "Romain", "Marie", "Chloe", "Jacques", "Emma", "Olivier", "Jessica"};
	int i = rand() % 10;

	e.setZombieType("Deadly");
	Zombie *z = e.newZombie(tab[i]);
	z->announce();
	delete z;
}

int main()
{
	Zombie z;

	srand(time(NULL));
	randomChump();
	z.announce();
	return (0);
}
