// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Brain.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:31:35 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 11:00:18 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Brain.hpp"

Brain::Brain() {}

Brain::~Brain() {}

std::string
Brain::identify() const
{
	std::stringstream ss;
	long l = (long)this;
	std::string ret;

	ss << "0x";
	ss << std::hex;
	ss << std::uppercase;
	ss << l << std::endl;
	ss >> ret;
	return (ret);
}
