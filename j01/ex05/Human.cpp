// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Human.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:46:55 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 14:22:55 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Human.hpp"

Human::Human()
{
	this->_brain = new Brain();
}

Human::~Human()
{
	delete this->_brain;
}

const Brain&
Human::getBrain()
{
	return *(this->_brain);
}

std::string
Human::identify() const
{
	return (this->_brain->identify());
}

