// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Weapon.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:09:54 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 14:26:07 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Weapon.hpp"

Weapon::Weapon(std::string type):
	_type(type)
{}

Weapon::~Weapon() {}

const std::string&
Weapon::getType() const
{
	return (this->_type);
}

void
Weapon::setType(std::string type)
{
	this->_type = type;
}
