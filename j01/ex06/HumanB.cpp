// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   HumanB.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 11:28:00 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 11:33:30 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "HumanB.hpp"

HumanB::HumanB(std::string name):
	_name(name),
	_weapon(NULL)
{}

HumanB::~HumanB() {}

void
HumanB::setWeapon(Weapon& w)
{
	this->_weapon = &w;
}

void
HumanB::attack()
{
	std::cout << this->_name << " attacks with his " << this->_weapon->getType() << std::endl;
}
