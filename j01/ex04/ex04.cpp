// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ex04.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:23:51 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 14:18:45 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>

int main()
{
	std::string str("HI THIS IS BRAIN");
	std::string *p_str = &str;
	std::string &r_str = str;

	std::cout << "Display with pointer: " << *p_str << std::endl;
	std::cout << "Display with reference: " << r_str << std::endl;
	return (0);
}
