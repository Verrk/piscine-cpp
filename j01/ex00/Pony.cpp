// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Pony.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:15:02 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 09:16:11 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"

Pony::Pony()
{
	std::cout << "Pony created" << std::endl;
}

Pony::~Pony()
{
	std::cout << "Pony destroyed" << std::endl;
}
