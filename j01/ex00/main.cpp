// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:16:20 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 09:23:16 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Pony.hpp"
#include <iostream>

void ponyOnTheHeap()
{
	Pony *hp;

	std::cout << "Begining of ponyOnTheHeap function" << std::endl;
	hp = new Pony();
	delete hp;
	std::cout << "End of ponyOnTheHeap function" << std::endl;
	return ;
}

void ponyOnTheStack()
{
	Pony sp;

	std::cout << "Begining of ponyOnTheStack function" << std::endl;
	std::cout << "End of ponyOnTheStack function" << std::endl;
	return ;
}

int main()
{
	std::cout << "Call of ponyOnThHeap" << std::endl;
	ponyOnTheHeap();
	std::cout << "Call of ponyOnTheStack" << std::endl;
	ponyOnTheStack();
	std::cout << "End of main function" << std::endl;
	return (0);
}
