// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:31:15 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 10:13:42 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ZOMBIE_HPP
# define ZOMBIE_HPP

# include <iostream>

class Zombie
{
public:
	Zombie(std::string name = "Unknown", std::string type = "Unknown");
	~Zombie();
	void setName(std::string name);
	void announce();

private:
	std::string _name;
	std::string _type;
};

#endif
