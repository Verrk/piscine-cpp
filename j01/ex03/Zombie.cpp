// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Zombie.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 09:33:42 by cpestour          #+#    #+#             //
//   Updated: 2015/10/28 14:13:22 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Zombie.hpp"

Zombie::Zombie(std::string name, std::string type):
	_name(name),
	_type(type)
{}

Zombie::~Zombie()
{
	std::cout << "Zomnie " << this->_name << " get wrecked!" << std::endl;
}

void
Zombie::setName(std::string name)
{
	this->_name = name;
}

void
Zombie::announce()
{
	std::cout << "<" << this->_name << " (" << this->_type << ")> Braiiiiiiinnnssss..." << std::endl;
}
