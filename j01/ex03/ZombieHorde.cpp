// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ZombieHorde.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/06/16 10:04:49 by cpestour          #+#    #+#             //
//   Updated: 2015/06/16 10:19:56 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ZombieHorde.hpp"

ZombieHorde::ZombieHorde(int n):
	_nbZ(n)
{
	std::string tab[10] = {"Roger", "Clement", "Julie", "Romain", "Marie", "Chloe", "Jacques", "Emma", "Olivier", "Jessica"};

	srand(time(NULL));
	this->_zList = new Zombie[n];
	while (n--)
		this->_zList[n].setName(tab[rand() % 10]);
}

ZombieHorde::~ZombieHorde()
{
	delete [] this->_zList;
}

void
ZombieHorde::announce()
{
	int i;

	for (i = 0; i < this->_nbZ; i++)
		this->_zList[i].announce();
}

