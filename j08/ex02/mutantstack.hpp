// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   mutantstack.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 20:17:31 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 21:42:42 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MUTANTSTACK_HPP
# define MUTANTSTACK_HPP

# include <stack>
# include <iterator>

template <class T>
class MutantStack: public std::stack<T>
{
public:
	MutantStack() {};
	MutantStack(MutantStack const & src):
		std::stack<T>(src)
		{}
	MutantStack & operator=(MutantStack const & rhs);
	~MutantStack() {};

	class iterator
	{
	public:
		typedef T value_type;
		typedef T& reference;
		typedef T* pointer;
		typedef int difference_type;
		typedef std::bidirectional_iterator_tag iterator_category;
		iterator(pointer ptr): _ptr(ptr) {}
		iterator operator++() {iterator i = *this; _ptr--; return i;}
		iterator operator++(int) {_ptr--; return *this;}
		iterator operator--() {iterator i = *this; _ptr++; return i;}
		iterator operator--(int) {_ptr++; return *this;}
		reference operator*() {return *_ptr;}
		pointer operator->() {return _ptr;}
		bool operator==(const iterator& rhs) {return _ptr == rhs._ptr;}
		bool operator!=(const iterator& rhs) {return _ptr != rhs._ptr;}
	private:
		pointer _ptr;
	};

	class const_iterator
	{
	public:
		typedef T value_type;
		typedef T& reference;
		typedef T* pointer;
		typedef int difference_type;
		typedef std::bidirectional_iterator_tag iterator_category;
		const_iterator(pointer ptr): _ptr(ptr) {}
		const_iterator operator++() {const_iterator i = *this; _ptr--; return i;}
		const_iterator operator++(int) {_ptr--; return *this;}
		const_iterator operator--() {const_iterator i = *this; _ptr++; return i;}
		const_iterator operator--(int) {_ptr++; return *this;}
		const reference operator*() {return *_ptr;}
		const pointer operator->() {return _ptr;}
		bool operator==(const const_iterator& rhs) {return _ptr == rhs._ptr;}
		bool operator!=(const const_iterator& rhs) {return _ptr != rhs._ptr;}
	private:
		pointer _ptr;
	};

	iterator begin()
		{
			T &v = this->top();
			return (iterator(&v));
		}
	iterator end()
		{
			T &v = this->top();
			T *p = &v - this->size();
			return (iterator(p));
		}
	const_iterator begin() const
		{
			T &v = this->top();
			return (const_iterator(&v));
		}
	const_iterator end() const
		{
			T &v = this->top();
			T *p = &v - this->size();
			return (const_iterator(p));
		}
};

#endif
