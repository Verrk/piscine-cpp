// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   easyfind.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/11 21:23:16 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 06:12:41 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef EASYFIND_HPP
# define EASYFIND_HPP

# include <algorithm>

template<typename T>
typename T::iterator easyfind(T &cont, int i)
{
/*	typename T::iterator itb = cont.begin();
	typename T::iterator ite = cont.end();

	while (itb != ite && *itb != i)
		itb++;
		return itb;*/
	return std::find(cont.begin(), cont.end(), i);
}

#endif
