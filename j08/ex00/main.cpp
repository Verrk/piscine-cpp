// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/11 21:36:05 by cpestour          #+#    #+#             //
//   Updated: 2015/11/11 21:50:32 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "easyfind.hpp"
#include <list>
#include <iostream>

int main()
{
	std::list<int> l;
	std::list<int>::iterator it;

	l.push_front(21);
	l.push_front(42);
	l.push_front(15);
	it = easyfind(l, 42);
	if (it != l.end())
		std::cout << *it << " found!" << std::endl;
	else
		std::cout << "not found!" << std::endl;
	it = easyfind(l, 15);
	if (it != l.end())
		std::cout << *it << " found!" << std::endl;
	else
		std::cout << "not found!" << std::endl;
	it = easyfind(l, 150);
	if (it != l.end())
		std::cout << *it << " found!" << std::endl;
	else
		std::cout << "not found!" << std::endl;
}
