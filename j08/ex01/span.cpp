// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   span.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 06:18:25 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 06:48:38 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "span.hpp"

Span::Span(unsigned int n):
	_n(n)
{}

Span::~Span() {}

void
Span::addNumber(int i)
{
	if (_v.size() == _n)
		throw fullListException();
	_v.push_back(i);
}

int
Span::shortestSpan() const
{
	std::vector<int> v(_v);
	int tmp = std::numeric_limits<int>::max();

	if (_v.size() <= 1)
		throw spanException();
	std::sort(v.begin(), v.end());
	for (size_t i = 0; i + 1 < v.size(); i++)
	{
		if (v[i + 1] - v[i] < tmp)
			tmp = v[i + 1] - v[i];
		if (tmp == 0)
			return tmp;
	}
	return tmp;
}

int
Span::longestSpan() const
{
	std::vector<int> v(_v);

	if (_v.size() <= 1)
		throw spanException();
	std::sort(v.begin(), v.end());
	return v.back() - v.front();
}

const char *
Span::fullListException::what() const throw()
{
	return "the list is full";
}

const char *
Span::spanException::what() const throw()
{
	return "not enough number int the list";
}
