// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 06:35:59 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 06:54:43 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "span.hpp"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>

int main()
{
	srand(time(NULL));
	Span sp = Span(100);
	int r;
	std::vector<int> v;


	for (int i = 0; i < 100; i++)
	{
		r = rand();
		sp.addNumber(r);
		v.push_back(r);
	}
	std::cout << sp.shortestSpan() << std::endl;
	std::cout << sp.longestSpan() << std::endl;
	std::sort(v.begin(), v.end());
	for (size_t i = 0; i < v.size(); i++)
		std::cout << v[i] << std::endl;
}
