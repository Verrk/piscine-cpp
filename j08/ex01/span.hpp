// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   span.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 06:15:43 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 06:46:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SPAN_HPP
# define SPAN_HPP

# include <vector>
# include <algorithm>
# include <exception>
# include <limits>

class Span
{
public:
	Span();
	Span(unsigned int n);
	Span(Span const & src);
	Span & operator=(Span const & rhs);
	~Span();

	void addNumber(int i);
	int shortestSpan() const;
	int longestSpan() const;

	class fullListException: public std::exception
	{
		const char * what() const throw();
	};

	class spanException: public std::exception
	{
		const char * what() const throw();
	};

private:
	unsigned int _n;
	std::vector<int> _v;
};

#endif
