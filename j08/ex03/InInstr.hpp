// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   InInstr.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 21:58:25 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:26:12 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ININSTR_HPP
# define ININSTR_HPP

# include "IInstr.hpp"
# include <iostream>

class InInstr: public IInstr
{
public:
	InInstr() {}
	InInstr(InInstr const & src);
	InInstr & operator=(InInstr const & rhs);
	~InInstr() {}

	void execute(Mindopen &m) {std::cin >> m.getChar();}
};

#endif
