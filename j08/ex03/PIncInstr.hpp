// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PIncInstr.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 22:03:11 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:25:21 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PINCINSTR_HPP
# define PINCINSTR_HPP

# include "IInstr.hpp"

class PIncInstr: public IInstr
{
public:
	PIncInstr() {}
	PIncInstr(PIncInstr const & src);
	PIncInstr & operator=(PIncInstr const & rhs);
	~PIncInstr() {}

	void execute(Mindopen &m) {m.pInc();}
};


#endif
