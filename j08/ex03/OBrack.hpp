// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OBrack.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/13 07:47:11 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 09:56:33 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OBRACK_HPP
# define OBRACK_HPP

# include <queue>
# include <cstdlib>
# include "IInstr.hpp"
# include "BDecInstr.hpp"
# include "BIncInstr.hpp"
# include "InInstr.hpp"
# include "OutInstr.hpp"
# include "PDecInstr.hpp"
# include "PIncInstr.hpp"

class OBrack: public IInstr
{
public:
	OBrack() {}
	OBrack(OBrack const & src);
	OBrack & operator=(OBrack const & rhs);
	~OBrack() {while (!q.empty()) q.pop();}

	void parse(std::string str, int &i, Mindopen &m)
		{
			i++;
			while (str[i] && str[i] != ']')
			{
				if (str[i] == '-')
					q.push(new BDecInstr);
				else if (str[i] == '+')
					q.push(new BIncInstr);
				else if (str[i] == '<')
					q.push(new PDecInstr);
				else if (str[i] == '>')
					q.push(new PIncInstr);
				else if (str[i] == ',')
					q.push(new InInstr);
				else if (str[i] == '.')
					q.push(new OutInstr);
				else if (str[i] == '[')
				{
					OBrack *o = new OBrack;
					o->parse(str, i, m);
					q.push(o);
				}
				else
					m.error("bad token");
				i++;
			}
			if (!str[i])
				m.error("mismatch brackets");
		}

	void execute(Mindopen &m)
		{
			std::queue<IInstr*> tmp = q;

			while (!tmp.empty())
			{
				IInstr *ptr = tmp.front();
				OBrack *o = dynamic_cast<OBrack*>(ptr);
				if (o != NULL)
					while (*(m.p))
						ptr->execute(m);
				else
					ptr->execute(m);
				tmp.pop();
			}
		}

private:
	std::queue<IInstr*> q;
};

#endif
