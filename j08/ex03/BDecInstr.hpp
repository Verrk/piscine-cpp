// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   BDecInstr.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 21:49:55 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:25:43 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BDECINSTR_HPP
# define BDECINSTR_HPP

# include "IInstr.hpp"

class BDecInstr: public IInstr
{
public:
	BDecInstr() {}
	BDecInstr(BDecInstr const & src);
	BDecInstr & operator=(BDecInstr const & rhs);
	~BDecInstr() {}

	void execute(Mindopen &m) {m.bDec();}
};

#endif
