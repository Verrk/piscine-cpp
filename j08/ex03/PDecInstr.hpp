// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PDecInstr.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 22:01:27 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:25:18 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PDECINSTR_HPP
# define PDECINSTR_HPP

# include "IInstr.hpp"

class PDecInstr: public IInstr
{
public:
	PDecInstr() {}
	PDecInstr(PDecInstr const & src);
	PDecInstr & operator=(PDecInstr const & rhs);
	~PDecInstr() {}

	void execute(Mindopen &m) {m.pDec();}
};

#endif
