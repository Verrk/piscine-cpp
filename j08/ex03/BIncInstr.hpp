// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   BIncInstr.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 21:56:36 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:25:59 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BINCINSTR_HPP
# define BINCINSTR_HPP

# include "IInstr.hpp"

class BIncInstr: public IInstr
{
public:
	BIncInstr() {}
	BIncInstr(BIncInstr const & src);
	BIncInstr & operator=(BIncInstr const & rhs);
	~BIncInstr() {}

	void execute(Mindopen &m) {m.bInc();}
};

#endif
