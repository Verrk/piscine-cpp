// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Mindopen.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 21:44:35 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 09:21:52 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MINDOPEN_HPP
# define MINDOPEN_HPP

# include <queue>
# include <iostream>
# include <fstream>
# include <string>
# include <iomanip>
# include <cstdlib>

class IInstr;

class Mindopen
{
public:
	Mindopen();
	Mindopen(Mindopen const & src);
	Mindopen & operator=(Mindopen const & rhs);
	~Mindopen();

	void bInc();
	void bDec();
	void pInc();
	void pDec();
	char & getChar();

	void getStr(std::fstream &f);
	void parse();
	void execute();

	void error(const char *err);

	std::queue<IInstr*> q;
	char a[65536];
	char *p;
	std::string prog;
	int brack;
};

# include "IInstr.hpp"
# include "BDecInstr.hpp"
# include "BIncInstr.hpp"
# include "InInstr.hpp"
# include "OutInstr.hpp"
# include "PDecInstr.hpp"
# include "PIncInstr.hpp"
# include "OBrack.hpp"

#endif
