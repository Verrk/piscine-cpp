// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IInstr.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 21:47:11 by cpestour          #+#    #+#             //
//   Updated: 2015/11/12 23:25:29 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IINSTR_HPP
# define IINSTR_HPP

# include "Mindopen.hpp"

class IInstr
{
public:
	virtual ~IInstr() {}
	virtual void execute(Mindopen&) = 0;
};

#endif
