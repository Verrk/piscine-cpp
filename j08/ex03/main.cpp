// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 22:06:23 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 00:18:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Mindopen.hpp"
#include "IInstr.hpp"

void usage()
{
	std::cout << "./a.out [file]" << std::endl;
}

int main(int ac, char **av)
{
	std::fstream f;
	Mindopen m;

	if (ac == 2)
	{
		f.open(av[1]);
		if (f.fail())
		{
			std::cout << "fail" << std::endl;
			return -1;
		}
		m.getStr(f);
		f.close();
		m.parse();
		m.execute();
	}
	else
		usage();
}
