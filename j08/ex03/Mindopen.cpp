// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Mindopen.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 23:17:55 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 08:43:08 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Mindopen.hpp"

Mindopen::Mindopen()
{
	p = a;
}

Mindopen::~Mindopen() {}

void
Mindopen::bInc()
{
	(*p)++;
}

void
Mindopen::bDec()
{
	(*p)--;
}

void
Mindopen::pInc()
{
	p++;
}

void
Mindopen::pDec()
{
	p--;
}

char &
Mindopen::getChar()
{
	return *p;
}

void
Mindopen::getStr(std::fstream &f)
{
	char c;

	f >> c;
	while(!f.eof())
	{
		prog.append(1, c);
		f >> c;
	}
}

void
Mindopen::error(const char *err)
{
	std::cout << "error: " << err << std::endl;
	exit(-1);
}

void
Mindopen::parse()
{
	int i = 0;

	while (prog[i])
	{
		if (prog[i] == '-')
			q.push(new BDecInstr);
		else if (prog[i] == '+')
			q.push(new BIncInstr);
		else if (prog[i] == '<')
			q.push(new PDecInstr);
		else if (prog[i] == '>')
			q.push(new PIncInstr);
		else if (prog[i] == ',')
			q.push(new InInstr);
		else if (prog[i] == '.')
			q.push(new OutInstr);
		else if (prog[i] == '[')
		{
			OBrack *o = new OBrack;
			o->parse(prog, i, *this);
			q.push(o);
		}
		else if (prog[i] == ']')
			error("mismatch brackets");
		else
			error("bad token");
		i++;
	}
}

void
Mindopen::execute()
{
	while (!q.empty())
	{
		IInstr *ptr = q.front();
		OBrack *o = dynamic_cast<OBrack*>(ptr);
		if (o != NULL)
			while (*p)
				ptr->execute(*this);
		else
			ptr->execute(*this);
		q.pop();
	}
}
