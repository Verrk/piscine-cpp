// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   OutInstr.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/11/12 22:00:02 by cpestour          #+#    #+#             //
//   Updated: 2015/11/13 09:32:54 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef OUTINSTR_HPP
# define OUTINSTR_HPP

# include "IInstr.hpp"
# include <iostream>

class OutInstr: public IInstr
{
public:
	OutInstr() {}
	OutInstr(OutInstr const & src);
	OutInstr & operator=(OutInstr const & rhs);
	~OutInstr() {}

	void execute(Mindopen &m) {std::cout << m.getChar() << std::flush;}
};

#endif
