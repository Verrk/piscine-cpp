// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Peon.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 11:02:26 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 11:09:05 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PEON_HPP
# define PEON_HPP

# include <iostream>
# include "Victim.hpp"

class Peon: public Victim
{
public:
	Peon(std::string const & name);
	Peon(Peon const & src);
	Peon& operator=(Peon const & rhs);
	~Peon();

	virtual void getPolymorphed() const;

private:
	Peon();
};

#endif
