// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Victim.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 10:52:10 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 11:14:22 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef VICTIM_HPP
# define VICTIM_HPP

# include <iostream>

class Victim
{
public:
	Victim(std::string const & name);
	Victim(Victim const & src);
	Victim& operator=(Victim const & rhs);
	~Victim();

	std::string getName() const;

	virtual void getPolymorphed() const;

protected:
	std::string _name;
	Victim();
};

std::ostream & operator<<(std::ostream & o, Victim const & rhs);

#endif
