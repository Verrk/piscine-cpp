// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Sorcerer.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 10:26:22 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 10:51:18 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SORCERER_HPP
# define SORCERER_HPP

# include <iostream>
# include "Victim.hpp"

class Sorcerer
{
public:
	Sorcerer(std::string const & name, std::string const & title);
	Sorcerer(Sorcerer const & src);
	Sorcerer &operator=(Sorcerer const & rhs);
	~Sorcerer();

	std::string getName() const;
	std::string getTitle() const;

	void polymorph(Victim const & victim);

private:
	Sorcerer();
	std::string _name;
	std::string _title;
};

std::ostream & operator<<(std::ostream & o, Sorcerer const & rhs);

#endif
