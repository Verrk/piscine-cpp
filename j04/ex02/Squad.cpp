// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Squad.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/29 14:02:13 by cpestour          #+#    #+#             //
//   Updated: 2015/10/29 14:49:31 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Squad.hpp"

Squad::Squad():
	_count(0)
{
	_unit = new ISpaceMarine*[42];
}

Squad::Squad(Squad const & src)
{
	*this = src;
}

Squad &
Squad::operator=(Squad const & rhs)
{
	int i;

	for (i = 0; i < this->getCount(); i++)
		delete this->_unit[i];
	this->_count = rhs.getCount();
	for (i = 0; i < this->getCount(); i++)
		this->_unit[i] = rhs.getUnit(i);
	return (*this);
}

Squad::~Squad()
{
	int i;

	for (i = 0; i < this->getCount(); i++)
		delete this->_unit[i];
	delete [] this->_unit;
}

int
Squad::getCount() const
{
	return this->_count;
}

ISpaceMarine*
Squad::getUnit(int i) const
{
	if (i < 0 || i >= this->getCount())
		return NULL;
	return this->_unit[i];
}

int
Squad::push(ISpaceMarine* u)
{
	int i;

	if (u != NULL)
	{
		for (i = 0; i < this->getCount(); i++)
		{
			if (u == this->_unit[i])
				return this->_count;
		}
		this->_unit[this->_count] = u;
		this->_count++;
	}
	return this->_count;
}
