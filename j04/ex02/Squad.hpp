// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Squad.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/29 13:53:32 by cpestour          #+#    #+#             //
//   Updated: 2015/10/29 14:47:14 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SQUAD_HPP
# define SQUAD_HPP

# include "ISquad.hpp"

class Squad: public ISquad
{
public:
	Squad();
	Squad(Squad const & src);
	Squad & operator=(Squad const & rhs);
	virtual ~Squad();

	virtual int getCount() const;
	virtual ISpaceMarine* getUnit(int) const;
	virtual int push(ISpaceMarine*);

private:
	int _count;
	ISpaceMarine **_unit;
};

#endif
