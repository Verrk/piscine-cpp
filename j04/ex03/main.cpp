// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:53:52 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:18:58 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "IMateriaSource.hpp"
#include "MateriaSource.hpp"
#include "AMateria.hpp"
#include "Ice.hpp"
#include "Cure.hpp"
#include "ICharacter.hpp"
#include "Character.hpp"
#include <iostream>

int main()
{
	IMateriaSource * src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	ICharacter * verrk = new Character("verrk");

	AMateria * tmp;
	tmp = src->createMateria("ice");
	verrk->equip(tmp);
	tmp = src->createMateria("cure");
	verrk->equip(tmp);

	ICharacter * bob = new Character("bob");

	verrk->use(0, *bob);
	verrk->use(1, *bob);

	delete bob;
	delete verrk;
	delete src;
}
