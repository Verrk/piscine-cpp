// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:21:30 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:02:46 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Character.hpp"

Character::Character(std::string const & name):
	_name(name)
{
	_m = new AMateria*[4];
}

Character::Character(Character const & src)
{
	*this = src;
}

Character &
Character::operator=(Character const & rhs)
{
	int i;

	for (i = 0; i < 4; i++)
		if (this->_m[i])
			delete this->_m[i];
	for (i = 0; i < 4; i++)
		this->_m[i] = rhs.getMateria(i);
	return *this;
}

Character::~Character()
{
	int i;

	for (i = 0; i < 4; i++)
		if (_m[i] != NULL)
			delete _m[i];
	delete [] _m;
}

std::string const &
Character::getName() const
{
	return this->_name;
}

AMateria *
Character::getMateria(int idx) const
{
	return _m[idx];
}

void
Character::equip(AMateria * m)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		if (_m[i] == NULL)
		{
			_m[i] = m;
			return ;
		}
	}
	std::cout << "Iventory full" << std::endl;
}

void
Character::unequip(int idx)
{
	if (_m[idx] != NULL)
		_m[idx] = NULL;
}

void
Character::use(int idx, ICharacter & target)
{
	if (_m[idx] != NULL)
	{
		_m[idx]->use(target);
	}
}
