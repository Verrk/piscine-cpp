// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Ice.cpp                                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:06:54 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:03:20 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Ice.hpp"

Ice::Ice():
	AMateria("ice"),
	_xp(0)
{}

Ice::Ice(Ice const & src):
	AMateria("ice")
{
	*this = src;
}

Ice &
Ice::operator=(Ice const & rhs)
{
	this->_xp = rhs.getXP();
	return *this;
}

Ice::~Ice() {}

Ice *
Ice::clone() const
{
	return new Ice(*this);
}

void
Ice::use(ICharacter & target)
{
	AMateria::use(target);
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}
