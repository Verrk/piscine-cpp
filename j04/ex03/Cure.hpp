// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cure.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:07:49 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:08:13 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CURE_HPP
# define CURE_HPP

# include "AMateria.hpp"

class Cure: public AMateria
{
public:
	Cure();
	Cure(Cure const & src);
	Cure & operator=(Cure const & rhs);
	virtual ~Cure();

	virtual Cure * clone() const;
	virtual void use(ICharacter & target);

private:
	unsigned int _xp;
};

#endif
