// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MateriaSource.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:43:28 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:06:58 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "MateriaSource.hpp"

MateriaSource::MateriaSource()
{
	_m = new AMateria *[4];
}

MateriaSource::MateriaSource(MateriaSource const & src)
{
	*this = src;
}

MateriaSource &
MateriaSource::operator=(MateriaSource const & rhs)
{
	int i;

	for (i = 0; i < 4; i++)
		if (this->_m[i])
			delete this->_m[i];
	for (i = 0; i < 4; i++)
		this->_m[i] = rhs.getMateria(i);
	return *this;
}

MateriaSource::~MateriaSource()
{
	int i;

	for (i = 0; i < 4; i++)
		if (this->_m[i])
			delete this->_m[i];
	delete [] _m;
}

AMateria *
MateriaSource::getMateria(int idx) const
{
	return _m[idx];
}

void
MateriaSource::learnMateria(AMateria * m)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		if (_m[i] == NULL)
		{
			_m[i] = m;
			return ;
		}
	}
}

AMateria *
MateriaSource::createMateria(std::string const & type)
{
	int i;

	for (i = 0; i < 4; i++)
		if (_m[i]->getType() == type)
			return _m[i]->clone();
	return NULL;
}
