// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Cure.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:09:08 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:18:41 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Cure.hpp"

Cure::Cure():
	AMateria("cure"),
	_xp(0)
{}

Cure::Cure(Cure const & src):
	AMateria("cure")
{
	*this = src;
}

Cure &
Cure::operator=(Cure const & rhs)
{
	this->_xp = rhs.getXP();
	return *this;
}

Cure::~Cure() {}

Cure *
Cure::clone() const
{
	return new Cure(*this);
}

void
Cure::use(ICharacter & target)
{
	AMateria::use(target);
	std::cout << "* heals " << target.getName() << "'s wounds *" << std::endl;
}
