// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Ice.hpp                                            :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:03:47 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 18:06:34 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ICE_HPP
# define ICE_HPP

# include "AMateria.hpp"

class Ice: public AMateria
{
public:
	Ice();
	Ice(Ice const & src);
	Ice & operator=(Ice const & rhs);
	virtual ~Ice();

	virtual Ice * clone() const;
	virtual void use(ICharacter & target);

private:
	unsigned int _xp;
};

#endif
