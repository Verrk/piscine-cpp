// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:16:12 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:02:23 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include "ICharacter.hpp"

class Character: public ICharacter
{
public:
	Character(std::string const & name);
	Character(Character const & src);
	Character & operator=(Character const & rhs);
	virtual ~Character();

	virtual std::string const & getName() const;
	AMateria * getMateria(int idx) const;
	virtual void equip(AMateria * m);
	virtual void unequip(int idx);
	virtual void use(int idx, ICharacter & target);

private:
	Character();
	std::string _name;
	AMateria **_m;
};

#endif
