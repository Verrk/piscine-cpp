// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AMateria.cpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 17:56:15 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:01:13 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "AMateria.hpp"

AMateria::AMateria(std::string const & type):
	_type(type),
	_xp(0)
{}

AMateria::AMateria(AMateria const & src)
{
	*this = src;
}

AMateria &
AMateria::operator=(AMateria const & rhs)
{
	this->_type = rhs.getType();
	this->_xp = rhs.getXP();
	return *this;
}

AMateria::~AMateria() {}

std::string const &
AMateria::getType() const
{
	return this->_type;
}

unsigned int
AMateria::getXP() const
{
	return this->_xp;
}

void
AMateria::use(ICharacter & target)
{
	(void)target;
	this->_xp += 10;
}
