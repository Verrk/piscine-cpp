// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MateriaSource.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 18:39:31 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 19:19:40 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MATERIASOURCE_HPP
# define MATERIASOURCE_HPP

# include "IMateriaSource.hpp"

class MateriaSource: public IMateriaSource
{
public:
	MateriaSource();
	MateriaSource(MateriaSource const & src);
	MateriaSource & operator=(MateriaSource const & rhs);
	virtual ~MateriaSource();

	AMateria * getMateria(int idx) const;

	virtual void learnMateria(AMateria * m);
	virtual AMateria * createMateria(std::string const & type);

private:
	AMateria **_m;
};

#endif
