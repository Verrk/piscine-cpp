// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MiningBarge.hpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:22:36 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:43:44 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef MININGBARGE_HPP
# define MININGBARGE_HPP

# include "IMiningLaser.hpp"
# include "IAsteroid.hpp"

class MiningBarge
{
public:
	MiningBarge();
	MiningBarge(MiningBarge const & src);
	MiningBarge & operator=(MiningBarge const & rhs);
	~MiningBarge();

	IMiningLaser * getMiningLaser(int idx) const;

	void equip(IMiningLaser *);
	void mine(IAsteroid *) const;

private:
	IMiningLaser **_ml;
};

#endif
