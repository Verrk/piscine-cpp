// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:32:05 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 21:03:36 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "IMiningLaser.hpp"
#include "DeepCoreMiner.hpp"
#include "StripMiner.hpp"
#include "IAsteroid.hpp"
#include "BocalSteroid.hpp"
#include "AsteroBocal.hpp"
#include "MiningBarge.hpp"

int main()
{
	MiningBarge *mb = new MiningBarge();
	mb->equip(new StripMiner());
	mb->equip(new DeepCoreMiner());
	IAsteroid * a1 = new BocalSteroid();
	IAsteroid * a2 = new AsteroBocal();
	mb->mine(a1);
	mb->mine(a2);

	mb->equip(new DeepCoreMiner());
	mb->equip(new DeepCoreMiner());
	mb->equip(new DeepCoreMiner());

	delete a1;
	delete a2;
	delete mb;
}
