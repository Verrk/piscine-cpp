// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IMiningLaser.hpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:53:28 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:56:57 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IMININGLASER_HPP
# define IMININGLASER_HPP

class IAsteroid;

class IMiningLaser
{
public:
	virtual ~IMiningLaser() {}
	virtual void mine(IAsteroid*) = 0;
};

# include "IAsteroid.hpp"

#endif
