// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   StripMiner.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:54:47 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:52:40 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef STRIPMINER_HPP
# define STRIPMINER_HPP

# include "IMiningLaser.hpp"
# include "IAsteroid.hpp"
# include <iostream>

class IAsteroid;

class StripMiner: public IMiningLaser
{
public:
	StripMiner();
	StripMiner(StripMiner const & src);
	StripMiner & operator=(StripMiner const & rhs);
	virtual ~StripMiner();

	virtual void mine(IAsteroid * a);
};

#endif
