// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DeepCoreMiner.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:57:06 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:59:12 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "DeepCoreMiner.hpp"

DeepCoreMiner::DeepCoreMiner() {}

DeepCoreMiner::DeepCoreMiner(DeepCoreMiner const & src)
{
	*this = src;
}

DeepCoreMiner &
DeepCoreMiner::operator=(DeepCoreMiner const & rhs)
{
	(void)rhs;
	return *this;
}

DeepCoreMiner::~DeepCoreMiner() {}

void
DeepCoreMiner::mine(IAsteroid * a)
{
	std::cout << "* mining deep ... got " << a->beMined(this) << " ! *" << std::endl;
}
