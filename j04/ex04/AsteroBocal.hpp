// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AsteroBocal.hpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:11:11 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:20:53 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ASTEROBOCAL_HPP
# define ASTEROBOCAL_HPP

# include "IAsteroid.hpp"

class AsteroBocal: public IAsteroid
{
public:
	AsteroBocal();
	AsteroBocal(AsteroBocal const & src);
	AsteroBocal & operator=(AsteroBocal const & rhs);
	virtual ~AsteroBocal();

	virtual std::string getName() const;

	virtual std::string beMined(DeepCoreMiner * m) const;
	virtual std::string beMined(StripMiner * m) const;

private:
	std::string _name;
};

#endif
