// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   BocalSteroid.hpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:11:11 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:18:13 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef BOCALSTEROID_HPP
# define BOCALSTEROID_HPP

# include "IAsteroid.hpp"

class BocalSteroid: public IAsteroid
{
public:
	BocalSteroid();
	BocalSteroid(BocalSteroid const & src);
	BocalSteroid & operator=(BocalSteroid const & rhs);
	virtual ~BocalSteroid();

	virtual std::string getName() const;

	virtual std::string beMined(DeepCoreMiner * m) const;
	virtual std::string beMined(StripMiner * m) const;

private:
	std::string _name;
};

#endif
