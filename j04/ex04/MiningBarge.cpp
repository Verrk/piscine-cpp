// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   MiningBarge.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:25:49 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 21:04:26 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "MiningBarge.hpp"

MiningBarge::MiningBarge()
{
	_ml = new IMiningLaser*[4];
}

MiningBarge::MiningBarge(MiningBarge const & src)
{
	*this = src;
}

MiningBarge &
MiningBarge::operator=(MiningBarge const & rhs)
{
	int i;

	for (i = 0; i < 4; i++)
		if (_ml[i])
			delete _ml[i];
	for (i = 0; i < 4; i++)
		_ml[i] = rhs.getMiningLaser(i);
	return *this;
}

MiningBarge::~MiningBarge()
{
	int i;

	for (i = 0; i < 4; i++)
		if (_ml[i])
			delete _ml[i];
	delete [] _ml;
}

IMiningLaser *
MiningBarge::getMiningLaser(int idx) const
{
	return _ml[idx];
}

void
MiningBarge::equip(IMiningLaser * ml)
{
	int i;

	for (i = 0; i < 4; i++)
	{
		if (_ml[i] == NULL)
		{
			_ml[i] = ml;
			return ;
		}
	}
	delete ml;
	std::cout << "Iventory full!" << std::endl;
}

void
MiningBarge::mine(IAsteroid * a) const
{
	int i;

	for (i = 0; i < 4; i++)
		if (_ml[i])
			_ml[i]->mine(a);
}
