// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   BocalSteroid.cpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:15:40 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:38:23 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "BocalSteroid.hpp"

BocalSteroid::BocalSteroid():
	_name("BocalSteroid")
{}

BocalSteroid::BocalSteroid(BocalSteroid const & src)
{
	*this = src;
}

BocalSteroid &
BocalSteroid::operator=(BocalSteroid const & rhs)
{
	this->_name = rhs.getName();
	return *this;
}

BocalSteroid::~BocalSteroid() {}

std::string
BocalSteroid::getName() const
{
	return this->_name;
}

std::string
BocalSteroid::beMined(DeepCoreMiner * m) const
{
	(void)m;
	return "Zazium";
}

std::string
BocalSteroid::beMined(StripMiner * m) const
{
	(void)m;
	return "Kripte";
}
