// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   StripMiner.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:57:06 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:59:23 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "StripMiner.hpp"

StripMiner::StripMiner() {}

StripMiner::StripMiner(StripMiner const & src)
{
	*this = src;
}

StripMiner &
StripMiner::operator=(StripMiner const & rhs)
{
	(void)rhs;
	return *this;
}

StripMiner::~StripMiner() {}

void
StripMiner::mine(IAsteroid * a)
{
	std::cout << "* strip mining ... got " << a->beMined(this) << " ! *" << std::endl;
}
