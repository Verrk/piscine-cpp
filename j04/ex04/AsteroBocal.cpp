// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AsteroBocal.cpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:15:40 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:39:16 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "AsteroBocal.hpp"

AsteroBocal::AsteroBocal():
	_name("AsteroBocal")
{}

AsteroBocal::AsteroBocal(AsteroBocal const & src)
{
	*this = src;
}

AsteroBocal &
AsteroBocal::operator=(AsteroBocal const & rhs)
{
	this->_name = rhs.getName();
	return *this;
}

AsteroBocal::~AsteroBocal() {}

std::string
AsteroBocal::getName() const
{
	return this->_name;
}

std::string
AsteroBocal::beMined(DeepCoreMiner * m) const
{
	(void)m;
	return "Thorite";
}

std::string
AsteroBocal::beMined(StripMiner * m) const
{
	(void)m;
	return "Flavium";
}
