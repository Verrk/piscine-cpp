// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IAsteroid.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 20:05:03 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 21:01:35 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IASTEROID_HPP
# define IASTEROID_HPP

# include <iostream>

class DeepCoreMiner;
class StripMiner;

class IAsteroid
{
public:
	virtual ~IAsteroid() {}
	virtual std::string beMined(DeepCoreMiner *) const = 0;
	virtual std::string beMined(StripMiner *) const = 0;
	virtual std::string getName() const = 0;
};

# include "DeepCoreMiner.hpp"
# include "StripMiner.hpp"

#endif
