// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   DeepCoreMiner.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/30 19:54:47 by cpestour          #+#    #+#             //
//   Updated: 2015/10/30 20:52:36 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef DEEPCOREMINER_HPP
# define DEEPCOREMINER_HPP

# include "IMiningLaser.hpp"
# include <iostream>

class IAsteroid;

class DeepCoreMiner: public IMiningLaser
{
public:
	DeepCoreMiner();
	DeepCoreMiner(DeepCoreMiner const & src);
	DeepCoreMiner & operator=(DeepCoreMiner const & rhs);
	virtual ~DeepCoreMiner();

	virtual void mine(IAsteroid * a);
};

#endif
