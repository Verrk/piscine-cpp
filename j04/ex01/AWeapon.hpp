// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Aweapon.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 14:22:46 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:54:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef AWEAPON_HPP
# define AWEAPON_HPP

# include <iostream>

class AWeapon
{
public:
	AWeapon(std::string const & name, int apcost, int damage);
	AWeapon(AWeapon const & src);
	AWeapon & operator=(AWeapon const & rhs);
	~AWeapon();

	std::string const & getName() const;
	int getAPCost() const;
	int getDamage() const;

	virtual void attack() const = 0;

protected:
	AWeapon();
	std::string _name;
	int _apCost;
	int _damage;
};

#endif
