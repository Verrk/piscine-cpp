// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   SuperMutant.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 15:02:04 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:07:30 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "SuperMutant.hpp"

SuperMutant::SuperMutant():
	Enemy(170, "Super Mutant")
{
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant::SuperMutant(SuperMutant const & src):
	Enemy(src)
{
	std::cout << "Gaaah. Me want smash heads !" << std::endl;
}

SuperMutant&
SuperMutant::operator=(SuperMutant const & rhs)
{
	this->_hp = rhs.getHP();
	this->_type = rhs.getType();
	return (*this);
}

SuperMutant::~SuperMutant()
{
	std::cout << "Aaargh ..." << std::endl;
}

void
SuperMutant::takeDamage(int amount)
{
	Enemy::takeDamage(amount - 3);
}
