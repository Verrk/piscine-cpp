// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 14:48:33 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 16:04:41 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENEMY_HPP
# define ENEMY_HPP

# include <iostream>

class Enemy
{
public:
	Enemy(int hp, std::string const & type);
	Enemy(Enemy const & src);
	Enemy & operator=(Enemy const & rhs);
	virtual ~Enemy();

	std::string const & getType() const;
	int getHP() const;

	virtual void takeDamage(int amount);

protected:
	Enemy();
	int _hp;
	std::string _type;
};

#endif
