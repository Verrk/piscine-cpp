// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   PowerFist.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 14:45:55 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 16:01:27 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "PowerFist.hpp"

PowerFist::PowerFist():
	AWeapon("Power Fist", 8, 50)
{}

PowerFist::PowerFist(PowerFist const & src):
	AWeapon(src)
{
	*this = src;
}

PowerFist&
PowerFist::operator=(PowerFist const & rhs)
{
	this->_name = rhs.getName();
	this->_apCost = rhs.getAPCost();
	this->_damage = rhs.getDamage();
	return (*this);
}

PowerFist::~PowerFist() {}

void
PowerFist::attack() const
{
	std::cout << "* pschhh... SBAM! *" << std::endl;
}
