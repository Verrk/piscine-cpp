// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 15:41:36 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 16:04:04 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Character.hpp"
#include "AWeapon.hpp"
#include "PlasmaRifle.hpp"
#include "PowerFist.hpp"
#include "Enemy.hpp"
#include "RadScorpion.hpp"
#include "SuperMutant.hpp"

int main()
{
	Character *verrk = new Character("Verrk");

	std::cout << *verrk;

	Enemy *b = new RadScorpion();

	AWeapon *pr = new PlasmaRifle();
	AWeapon *pf = new PowerFist();

	verrk->equip(pr);
	std::cout << *verrk;
	verrk->equip(pf);

	verrk->attack(b);
	std::cout << *verrk;
	verrk->equip(pr);
	std::cout << *verrk;
	verrk->attack(b);
	std::cout << *verrk;
	verrk->attack(b);
	std::cout << *verrk;
}
