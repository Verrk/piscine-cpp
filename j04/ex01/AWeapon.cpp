// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   AWeapon.cpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 14:26:49 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:55:10 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "AWeapon.hpp"

AWeapon::AWeapon(std::string const & name, int apcost, int damage):
	_name(name),
	_apCost(apcost),
	_damage(damage)
{}

AWeapon::AWeapon(AWeapon const & src)
{
	*this = src;
}

AWeapon&
AWeapon::operator=(AWeapon const & rhs)
{
	this->_name = rhs.getName();
	this->_apCost = rhs.getAPCost();
	this->_damage = rhs.getDamage();
	return (*this);
}

AWeapon::~AWeapon() {}

std::string const &
AWeapon::getName() const
{
	return (this->_name);
}

int
AWeapon::getAPCost() const
{
	return (this->_apCost);
}

int
AWeapon::getDamage() const
{
	return (this->_damage);
}
