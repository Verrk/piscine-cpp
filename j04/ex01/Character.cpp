// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.cpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 15:21:18 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 16:02:02 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Character.hpp"

Character::Character(std::string const & name):
	_name(name),
	_ap(40),
	_weapon(NULL)
{}

Character::Character(Character const & src)
{
	*this = src;
}

Character&
Character::operator=(Character const & rhs)
{
	this->_name = rhs.getName();
	this->_ap = rhs.getAP();
	this->_weapon = rhs.getWeapon();
	return (*this);
}

Character::~Character() {}

std::string const &
Character::getName() const
{
	return (this->_name);
}

int
Character::getAP() const
{
	return (this->_ap);
}

AWeapon*
Character::getWeapon() const
{
	return (this->_weapon);
}

void
Character::recoverAP()
{
	if (this->_ap >= 30)
		this->_ap = 40;
	else
		this->_ap += 10;
}

void
Character::equip(AWeapon * weapon)
{
	this->_weapon = weapon;
}

void
Character::attack(Enemy * target)
{
	if (this->_weapon && this->_weapon->getAPCost() <= this->_ap)
	{
		std::cout << this->_name << " attacks " << target->getType() << " with a " << this->_weapon->getName() << std::endl;
		this->_weapon->attack();
		target->takeDamage(this->_weapon->getDamage());
		if (target->getHP() <= 0)
			delete target;
		this->_ap -= this->_weapon->getAPCost();
	}
}

std::ostream &
operator<<(std::ostream & o, Character const & rhs)
{
	o << rhs.getName() << " has " << rhs.getAP() << " AP and ";
	if (rhs.getWeapon())
		o << "wields a " << rhs.getWeapon()->getName() << std::endl;
	else
		o << "is unarmed" << std::endl;
	return (o);
}
