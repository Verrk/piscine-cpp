// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   RadScorpion.cpp                                    :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 15:11:45 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:13:30 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "RadScorpion.hpp"

RadScorpion::RadScorpion():
	Enemy(80, "RadScorpion")
{
	std::cout << "* click click click *" << std::endl;
}

RadScorpion::RadScorpion(RadScorpion const & src):
	Enemy(src)
{
	std::cout << "* click click click *" << std::endl;
}

RadScorpion&
RadScorpion::operator=(RadScorpion const & rhs)
{
	this->_hp = rhs.getHP();
	this->_type = rhs.getType();
	return (*this);
}

RadScorpion::~RadScorpion()
{
	std::cout << "* SPROTCH *" << std::endl;
}

void
RadScorpion::takeDamage(int amount)
{
	Enemy::takeDamage(amount);
}
