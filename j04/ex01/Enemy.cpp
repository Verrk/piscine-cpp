// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 14:52:33 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:34:24 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Enemy.hpp"

Enemy::Enemy(int hp, std::string const & type):
	_hp(hp),
	_type(type)
{}

Enemy::Enemy(Enemy const & src)
{
	*this = src;
}

Enemy&
Enemy::operator=(Enemy const & rhs)
{
	this->_hp = rhs.getHP();
	this->_type = rhs.getType();
	return (*this);
}

Enemy::~Enemy() {};

std::string const &
Enemy::getType() const
{
	return (this->_type);
}

int
Enemy::getHP() const
{
	return (this->_hp);
}

void
Enemy::takeDamage(int amount)
{
	if (amount > 0)
		this->_hp -= amount;
}
