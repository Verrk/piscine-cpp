// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Character.hpp                                      :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/10/26 15:14:35 by cpestour          #+#    #+#             //
//   Updated: 2015/10/26 15:54:32 by cpestour         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef CHARACTER_HPP
# define CHARACTER_HPP

# include <iostream>
# include "AWeapon.hpp"
# include "Enemy.hpp"

class Character
{
public:
	Character(std::string const & name);
	Character(Character const & src);
	Character & operator=(Character const & rhs);
	~Character();

	std::string const & getName() const;
	int getAP() const;
	AWeapon * getWeapon() const;

	void recoverAP();
	void equip(AWeapon * weapon);
	void attack(Enemy * target);

private:
	Character();
	std::string _name;
	int _ap;
	AWeapon * _weapon;
};

std::ostream & operator<<(std::ostream & o, Character const & rhs);

#endif
